﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventCreater
{
    /// <summary>
    /// 個々のイベント編集の親クラス。
    /// </summary>
    public class EventContentsForm
    {
        Form myForm = null;
        int isEdit = 0;
        public EventContentsForm(Form f, int mode = 0)
        {
            isEdit = mode;
            myForm = f;
            // mode = 0:イベント行動選択リストから開いている。
            // mode = 1:イベントリストから直接編集している。
            if (mode == 0)
                parentForm = EventActListForm.eventActListForm;
            else
                parentForm = null;
        }

        /// <summary>
        /// イベントのコンテンツの選択フォームが入る。コンテンツ画面を閉じる用。
        /// </summary>
        public Form parentForm = null;
        
        /// <summary>
        /// codeを保存して閉じる。親フォームともども消滅させる。
        /// (普通に閉じるだけならthis.Close()のみ)
        /// </summary>
        public void buttonOK_Close(string[] code)
        {
            EventData.SetCodeFromEventActList(code, isEdit);
            myForm.Close();
            if (parentForm != null)
            {
                parentForm.Close();
                parentForm = null;
            }
        }

        /// <summary>
        /// キャンセルで終了。
        /// </summary>
        public void buttonCansel_Close()
        {
            myForm.Close();
        }
    }
}
