﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class NoteForm : Form
    {
        EventContentsForm ecf;
        public NoteForm(Form parent, int mode = 0, string[] e_St = null, int[] e_Args = null)
        {
            // 個々のイベントコンテンツの共通処理
            InitializeComponent();

            ecf = new EventContentsForm(this, mode);
            SetArgsToForm(e_St);
        }

        private void SetArgsToForm(string[] ags)
        {
            if (ags == null) return;
            textBox1.Lines = ags;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<string> code = new List<string>();
            code.Add("Note");

            string[] strs = textBox1.Text.Split('\n');
            for (int i = 0; i < strs.Length; i++)
            {
                int idx = strs[i].IndexOf('\r');
                if (idx != -1) strs[i] = strs[i].Remove(idx, 1);
            }

            code[0] = code[0] + "(\"" + strs[0] + "\")";

            for (int i = 1; i < strs.Length; i++)
                code.Add("SubN(" + "\"" + strs[i] + "\")");

            ecf.buttonOK_Close(code.ToArray());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ecf.buttonCansel_Close();
        }
        
        private string befstrbkup = "";
        // 文字"/"の入力禁止
        private void shopInTextBox_TextChanged(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text.IndexOf('\"') != -1)
            {
                MessageBox.Show("'\"'は入力できません。");
                tb.Text = befstrbkup;
            }
        }

        private void shopInTextBox_Enter(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            befstrbkup = tb.Text;
        }

        private bool shift = false;
        private bool ctrl = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            // 誤動作防止
            if (Form.ActiveForm != this)
                return;

            shift = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftShift) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightShift);
            ctrl = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl);

            if (shift == true && ctrl == true)
                button1_Click(null, null);
        }
    }
}
