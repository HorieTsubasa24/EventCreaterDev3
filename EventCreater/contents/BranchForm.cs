﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class BranchForm : Form
    {
        int[] Args = new int[6];
        EventContentsForm ecf;
        ComponentsSync arsyn;
        public BranchForm(Form parent, int mode = 0, int[] e_Args = null)
        {
            // 個々のイベントコンテンツの共通処理
            InitializeComponent();

            ecf = new EventContentsForm(this, mode);
            ecf.parentForm = parent;

            Item.ItemLoadAndSet(comboBoxitem);
            //Item.SkillLoadAndSet(comboBoxskill);

            comboBoxflg.SelectedIndex = 0;
            comboBoxGroupValCond.SelectedIndex = 0;
            comboBoxmon.SelectedIndex = 0;
            comboBoxitem.SelectedIndex = 0;
            comboBoxitemCond.SelectedIndex = 0;
            comboBoxActor.SelectedIndex = 0;
            comboActorCond.SelectedIndex = 0;
            comboBoxCharaDirection.SelectedIndex = 0;
            
            object[] radios = { radioButton1, radioButton1, radioButton1,
                                radioButton2, radioButton2,radioButton2,
                                radioButton3, radioButton3,
                                radioButton4, radioButton4,
                                radioButton5,
                                radioButton6,
                                radioButton10,
                                radioButton11, radioButton11,
                                radioButton5
            };

            object[] boxs = { labelflg, buttonflg, comboBoxflg,
                                labelval, buttonval, groupBoxVal,
                                numericUpDownmon, comboBoxmon,
                                comboBoxitem, comboBoxitemCond,
                                comboBoxActor, 
                                comboBoxCharaDirection,
                                numericUpDownGroupval,
                                labelGroupval, buttonGroupVal,
                                groupBoxActor
            };
            
            arsyn = new ComponentsSync(radios, boxs);
            SetArgsToForm(e_Args);
        }

        private void SetArgsToForm(int[] ags)
        {
            if (ags == null) return;
            switch (ags[0])
            {
                case 0:
                    radioButton1.Checked = true;
                    labelflg.Text = Variable.GetFlagLabelText(ags[1]);
                    comboBoxflg.SelectedIndex = ags[2];
                    break;
                case 1:
                    radioButton2.Checked = true;
                    labelval.Text = Variable.GetVariableLabelText(ags[1]);
                    if (ags[2] == 0)
                    {
                        radioButton10.Checked = true;
                        numericUpDownGroupval.Value = ags[3];
                    }
                    else
                    {
                        radioButton11.Checked = true;
                        labelGroupval.Text = Variable.GetVariableLabelText(ags[3]);
                    }
                    comboBoxGroupValCond.SelectedIndex = ags[4];
                    break;
                case 3:
                    radioButton3.Checked = true;
                    numericUpDownmon.Value = ags[1];
                    comboBoxmon.SelectedIndex = ags[2];
                    break;
                case 4:
                    radioButton4.Checked = true;
                    comboBoxitem.SelectedIndex = ags[1];
                    comboBoxitemCond.SelectedIndex = ags[2];
                    break;
                case 5:
                    radioButton5.Checked = true;
                    comboBoxActor.SelectedIndex = ags[1];
                    comboActorCond.SelectedIndex = ags[2];
                    comboBoxContentsEnableAndSet();
                    if (comboBoxActorCondSecond.Enabled == true)
                        comboBoxActorCondSecond.SelectedIndex = ags[3];
                    else
                        numericUpDownActor.Value = ags[3];
                    break;
                case 6:
                    radioButton6.Checked = true;
                    comboBoxCharaDirection.SelectedIndex = ags[1];
                    break;
            }
            if (ags[5] == 1) checkBox1.Checked = true;

        }

        private void buttonflg_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(0, labelflg, (int a, string vnamecomp) =>
            {
                labelflg.Text = vnamecomp;
            });
            CloseAct.FormOpenCommon(this, vf);
        }

        private void buttonval_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(1, labelval, (int a, string vnamecomp) =>
            {
                labelval.Text = vnamecomp;
            });
            CloseAct.FormOpenCommon(this, vf);
        }

        private void buttonGroupVal_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(1, labelGroupval, (int a, string vnamecomp) =>
            {
                labelGroupval.Text = vnamecomp;
            });
            CloseAct.FormOpenCommon(this, vf);
        }
        
        private void button6_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Args[0] = (radioButton1.Checked == true) ? 0 : Args[0];
            Args[0] = (radioButton2.Checked == true) ? 1 : Args[0];
            Args[0] = (radioButton3.Checked == true) ? 3 : Args[0];
            Args[0] = (radioButton4.Checked == true) ? 4 : Args[0];
            Args[0] = (radioButton5.Checked == true) ? 5 : Args[0];
            Args[0] = (radioButton6.Checked == true) ? 6 : Args[0];

            Args[1] = (radioButton1.Checked == true) ? Variable.GetVariableIndex(labelflg) : Args[1];
            Args[1] = (radioButton2.Checked == true) ? Variable.GetVariableIndex(labelval) : Args[1];
            Args[1] = (radioButton3.Checked == true) ? (int)numericUpDownmon.Value : Args[1];
            Args[1] = (radioButton4.Checked == true) ? comboBoxitem.SelectedIndex : Args[1];
            Args[1] = (radioButton5.Checked == true) ? comboBoxActor.SelectedIndex : Args[1];
            Args[1] = (radioButton6.Checked == true) ? comboBoxCharaDirection.SelectedIndex : Args[1];

            Args[2] = (radioButton1.Checked == true) ? comboBoxflg.SelectedIndex : Args[2];
            Args[2] = (radioButton2.Checked == true) ? Convert.ToInt32(radioButton11.Checked) : Args[2];
            Args[2] = (radioButton3.Checked == true) ? comboBoxmon.SelectedIndex : Args[2];
            Args[2] = (radioButton4.Checked == true) ? comboBoxitemCond.SelectedIndex : Args[2];
            
            if (radioButton2.Checked)
            {
                if(radioButton10.Checked == true)
                {
                    Args[3] = (int)numericUpDownGroupval.Value;
                    Args[4] = comboBoxGroupValCond.SelectedIndex;
                }
                else
                {
                    Args[3] = Variable.GetVariableIndex(labelGroupval);
                    Args[4] = comboBoxGroupValCond.SelectedIndex;
                }
            }
            
            if(radioButton5.Checked == true)
            {
                //0,1 : 2,3,
                Args[2] = comboActorCond.SelectedIndex;
                Args[3] = (comboBoxActorCondSecond.Enabled == true) ? comboBoxActorCondSecond.SelectedIndex : (int)numericUpDownActor.Value;
            }

            Args[5] = Convert.ToInt32(checkBox1.Checked);

            string[] code = new string[3 + Args[5] * 2];
            for (int i = 0; i < code.Length; i++)
                code[i] = "";

            string str = "";

            str = (radioButton1.Checked == true) ? "\"フラグ条件_" + labelflg.Text : str;
            str = (radioButton2.Checked == true) ? "\"変数条件_" + labelval.Text : str;
            str = (radioButton3.Checked == true) ? "\"所持金_" + numericUpDownmon.Value + comboBoxmon.SelectedItem: str;
            str = (radioButton4.Checked == true) ? "\"アイテム_" + comboBoxitem.SelectedItem + comboBoxitemCond.SelectedItem : str;
            str = (radioButton5.Checked == true) ? "\"主人公_" + comboBoxActor.SelectedItem : str;
            str = (radioButton6.Checked == true) ? "\"操作キャラ_" + comboBoxCharaDirection.SelectedItem + "向き": str;
            str += "\",";


            code[0] = "If(" + str + Args[0] + "," + Args[1] + "," + Args[2] + "," + Args[3] + "," + Args[4] + "," + Args[5] + ")";
            if (Args[5] == 1)
            {
                code[2] = "Else:";
                code[4] = "Endif:";
            }
            else
                code[2] = "Endif:";

            ecf.buttonOK_Close(code);
        }
        

        /// <summary>
        /// comboboxActorの変更で有効化するコンポーネントと、コンポボックスの内容を変える。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboActorCond_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxContentsEnableAndSet();
        }

        private void comboBoxContentsEnableAndSet()
        {
            if (comboActorCond.SelectedIndex == 2 ||
                comboActorCond.SelectedIndex == 3)
            {
                comboBoxActorCondSecond.Enabled = false;
                numericUpDownActor.Enabled = true;
            }
            else if (comboActorCond.SelectedIndex >= 4)
            {
                comboBoxContentsFix();
                comboBoxActorCondSecond.Enabled = true;
                numericUpDownActor.Enabled = false;
            }
            else
            {
                comboBoxActorCondSecond.Items.Clear();
                comboBoxActorCondSecond.Text = "";
                comboBoxActorCondSecond.Enabled = false;
                numericUpDownActor.Enabled = false;
            }
        }

        private void comboBoxContentsFix()
        {
            comboBoxActorCondSecond.Items.Clear();
            string[] strs;
            if (comboActorCond.SelectedIndex == 4)
                strs = Item.ReaditemFile(Item.fname[1]);
            else if (comboActorCond.SelectedIndex == 5)
                strs = Item.ReaditemFile(Item.fname[0]);
            else // if (comboActorCond.SelectedIndex == 6)
                strs = Variable.ReadVariableFile(Variable.fname[2]);

            comboBoxActorCondSecond.Items.AddRange(strs);
        }

        private bool shift = false;
        private bool ctrl = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            // 誤動作防止
            if (Form.ActiveForm != this)
                return;

            shift = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftShift) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightShift);
            ctrl = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl);

            if (shift == true && ctrl == true)
                button5_Click(null, null);
        }
    }
}
