﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class CharaName : Form
    {
        EventContentsForm ecf;
        public CharaName(Form parent, int mode = 0, string e_St = null, int[] e_Args = null)
        {
            InitializeComponent();
            ecf = new EventContentsForm(this, mode);
            SetArgsToForm(e_St);
        }

        private void SetArgsToForm(string ags)
        {
            if (ags == null) return;
            string[] s_ags ={ ags };
            textBox1.Lines = s_ags;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string[] code = new string[1];
            code[0] = "CharaName(\"" + textBox1.Text + "\")";

            ecf.buttonOK_Close(code);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ecf.buttonCansel_Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private bool shift = false;
        private bool ctrl = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            // 誤動作防止
            if (Form.ActiveForm != this)
                return;

            shift = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftShift) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightShift);
            ctrl = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl);

            if (shift == true && ctrl == true)
                button1_Click(null, null);
        }
    }
}
