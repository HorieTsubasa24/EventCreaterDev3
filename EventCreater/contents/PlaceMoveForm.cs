﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class PlaceMoveForm : Form
    {
        EventContentsForm ecf;
        ComponentsSync arsyn;
        public PlaceMoveForm(Form parent, int mode = 0, int[] e_Args = null)
        {
            InitializeComponent();
            SetArgs(e_Args);


            object[] radios = 
            {
                radioButton1, radioButton1, radioButton1, radioButton1,
                radioButton2, radioButton2, radioButton2, radioButton2,
                radioButton2, radioButton2, radioButton2
            };

            object[] boxs = 
            {
                numericUpDown4, numericUpDown1, numericUpDown2, numericUpDown3,
                labelhr, labelfl, labelx, labely,
                buttonval, button1, button2
            };

            arsyn = new ComponentsSync(radios, boxs);
            arsyn.async();
            ecf = new EventContentsForm(this, mode);
        }
        
        private void SetArgs(int[] args)
        {
            if (args == null || args.Length < 5)
                return;

            if (args[0] == 0)
            {
                radioButton1.Checked = true;
                numericUpDown4.Value = args[1];
                numericUpDown1.Value = args[2];
                numericUpDown2.Value = args[3];
                numericUpDown3.Value = args[4];
            }
            else
            {
                radioButton2.Checked = true;
                labelhr.Text = Variable.GetVariableLabelText(args[1]);
                labelfl.Text = Variable.GetVariableLabelText(args[2]);
                labelx.Text = Variable.GetVariableLabelText(args[3]);
                labely.Text = Variable.GetVariableLabelText(args[4]);
            }
        }

        private void buttonval_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(1, labelfl, (int a, string vnamecomp) =>
            {
                labelfl.Text = vnamecomp;
            });
            CloseAct.FormOpenCommon(this, vf);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(1, labelx, (int a, string vnamecomp) =>
            {
                labelx.Text = vnamecomp;
            });
            CloseAct.FormOpenCommon(this, vf);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(1, labely, (int a, string vnamecomp) =>
            {
                labely.Text = vnamecomp;
            });
            CloseAct.FormOpenCommon(this, vf);
        }

        private void buttonHr_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(1, labelhr, (int a, string vnamecomp) =>
            {
                labelhr.Text = vnamecomp;
            });
            CloseAct.FormOpenCommon(this, vf);
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            int select = (radioButton2.Checked == true) ? 1 : 0;
            int ag1;
            int ag2;
            int ag3;
            int ag4;
            if (select == 0)
            {
                ag1 = (int)numericUpDown4.Value; // hr
                ag2 = (int)numericUpDown1.Value; // fl
                ag3 = (int)numericUpDown2.Value; // x
                ag4 = (int)numericUpDown3.Value; // y
            }
            else
            {
                ag1 = Variable.GetVariableIndex(labelhr);
                ag2 = Variable.GetVariableIndex(labelfl);
                ag3 = Variable.GetVariableIndex(labelx);
                ag4 = Variable.GetVariableIndex(labely);
            }

            string[] code = { "MovePlace(" + select + "," + ag1 + "," + ag2 + "," + ag3 + "," + ag4 + ")" };
            ecf.buttonOK_Close(code);
        }

        private void buttonCansel_Click(object sender, EventArgs e)
        {
            ecf.buttonCansel_Close();
        }

        private bool shift = false;
        private bool ctrl = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            // 誤動作防止
            if (Form.ActiveForm != this)
                return;

            shift = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftShift) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightShift);
            ctrl = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl);

            if (shift == true && ctrl == true)
                buttonOK_Click(null, null);
        }
    }
}
