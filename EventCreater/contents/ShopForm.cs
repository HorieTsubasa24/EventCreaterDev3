﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class ShopForm : Form
    {
        EventContentsForm ecf;
        public ShopForm(Form parent, int mode = 0, string Str = null, int[] e_Args = null)
        {
            InitializeComponent();
            ArgsSet(Str, e_Args);

            ecf = new EventContentsForm(this, mode);
        }

        private void ArgsSet(string str, int[] args)
        {
            if (str == null)
                return;

            // length = 16
            string[] vs = str.Split('/');
            TextBox[] tb = 
            {
                shopInTextBox, shopTextBox, shopEndTextBox, buyTextBox, sellTextBox, retTextBox,
                buySelectTextBox, selectItemBuyTextBox, whenBuyTextBox, whenNoBuyTextBox, whenItemOverTextBox,
                sellSelectTextBox, selectItemSellTextBox, whenSellTextBox, whenNoSellTextBox, whenCannotSellTextBox
            };
            for (int i = 0; i < tb.Length; i++)
                tb[i].Text = vs[i];

            Item.ItemListLoad();
            foreach (var v in args)
                SellItemsListBox.Items.Add(Item.GetItemLabelText(v));
        }


        private void AddItemButton_Click(object sender, EventArgs e)
        {
            ItemForm itf = new ItemForm(0, (int a, string inamecomp) =>
            {
                ListBox lb = SellItemsListBox;
                int insertIdx = lb.SelectedIndex;
                if (insertIdx == -1 || lb.Items.Count <= insertIdx)
                    insertIdx = lb.Items.Count;
                lb.Items.Insert(insertIdx, inamecomp);
            });

            CloseAct.FormOpenCommon(this, itf);
        }

        private void SubItemButton_Click(object sender, EventArgs e)
        {
            ListBox lb = SellItemsListBox;
            int delIdx = lb.SelectedIndex;
            if (delIdx == -1 || lb.Items.Count <= delIdx)
                return;

            lb.Items.RemoveAt(delIdx);
            if (delIdx < lb.Items.Count)
                lb.SelectedIndex = delIdx;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            string[] code = { "Shop(\"" };
            code[0] += shopInTextBox.Text + '/';
            code[0] += shopTextBox.Text + '/';
            code[0] += shopEndTextBox.Text + '/';

            code[0] += buyTextBox.Text + '/';
            code[0] += sellTextBox.Text + '/';
            code[0] += retTextBox.Text + '/';

            code[0] += buySelectTextBox.Text + '/';
            code[0] += selectItemBuyTextBox.Text + '/';
            code[0] += whenBuyTextBox.Text + '/';
            code[0] += whenNoBuyTextBox.Text + '/';
            code[0] += whenItemOverTextBox.Text + '/';

            code[0] += sellSelectTextBox.Text + '/';
            code[0] += selectItemSellTextBox.Text + '/';
            code[0] += whenSellTextBox.Text + '/';
            code[0] += whenNoSellTextBox.Text + '/';
            code[0] += whenCannotSellTextBox.Text + "/";

            for (int i = 0; i < SellItemsListBox.Items.Count; i++)
                code[0] += SellItemsListBox.Items[i].ToString().Split(':')[1] + "/";
            code[0] += "\"";
            for (int i = 0; i < SellItemsListBox.Items.Count; i++)
                code[0] +=  "," + Item.GetVariableIndex((string)SellItemsListBox.Items[i]).ToString();

            code[0] += ")";
            ecf.buttonOK_Close(code);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            ecf.buttonCansel_Close();
        }


        private string befstrbkup = "";
        // 文字"/"の入力禁止
        private void shopInTextBox_TextChanged(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text.IndexOf('/') != -1 || tb.Text.IndexOf('\"') != -1)
            {
                MessageBox.Show("'/', '\"'は入力できません。");
                tb.Text = befstrbkup;
            }
        }

        private void shopInTextBox_Enter(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            befstrbkup = tb.Text;
        }

        private bool shift = false;
        private bool ctrl = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            // 誤動作防止
            if (Form.ActiveForm != this)
                return;

            shift = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftShift) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightShift);
            ctrl = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl);

            if (shift == true && ctrl == true)
                buttonOK_Click(null, null);
        }
    }
}
