﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class BattleForm : Form
    {
        int[] Args = new int[6];
        EventContentsForm ecf;
        ComponentsSync arsyn;
        public BattleForm(Form parent, int mode = 0, string e_St = null, int[] e_Args = null)
        {
            InitializeComponent();

            ecf = new EventContentsForm(this, mode);
            ecf.parentForm = parent;

            Variable.BattleCharaLoadAndSet(comboBox1);
            Variable.BattleFieldLoadAndSet(comboBox3);

            RadioButton[] radios = { radioButton1, radioButton2, radioButton2 };
            Component[] boxes = { comboBox1, labelval, buttonval };

            arsyn = new ComponentsSync(radios, boxes);
            SetArgsToForm(e_Args, e_St);
        }

        private void SetArgsToForm(int[] ags, string str) {
            if (ags == null)
                return;

            if (ags[0] == 0) {
                radioButton1.Checked = true;
                comboBox1.SelectedIndex = ags[1];
            } else {
                radioButton2.Checked = true;
                labelval.Text = Variable.GetFlagLabelText(ags[1]);
            }
            comboBox3.SelectedIndex = ags[2];
            if (ags[3] == 0) {
                radioButton5.Checked = true;
            } else if (ags[3] == 1) {
                radioButton6.Checked = true;
            } else {
                radioButton7.Checked = true;
            }

            if (ags[4] == 0) {
                radioButton8.Checked = true;
            } else {
                radioButton9.Checked = true;
            }

            checkBox1.Checked = (ags[5] == 1);
        }

        private void buttonOK_Click(object sender, EventArgs e) {

            string[] code = new string[1];

            string str = "";
            Args[0] = (radioButton1.Checked == true) ? 0 : 1;
            str = (Args[0] == 0) ? comboBox1.Text : "";

            Args[1] = (Args[0] == 0) ? comboBox1.SelectedIndex : Variable.GetVariableIndex(labelval);

            Args[2] = comboBox3.SelectedIndex;

            Args[3] = (radioButton5.Checked == true) ? 0 : Args[3];
            Args[3] = (radioButton6.Checked == true) ? 1 : Args[3];
            Args[3] = (radioButton7.Checked == true) ? 2 : Args[3];

            Args[4] = (radioButton8.Checked == true) ? 0 : Args[4];
            Args[4] = (radioButton9.Checked == true) ? 1 : Args[4];

            Args[5] = Convert.ToInt32(checkBox1.Checked);

            
            code[0] = "Battle(\"" + str + "\"," + Args[0] + "," + Args[1] + "," + Args[2] + "," + Args[3] + "," + Args[4] + "," + Args[5] + ")";
            ecf.buttonOK_Close(code);
        }

        private void buttonCansel_Click(object sender, EventArgs e) {
            ecf.buttonCansel_Close();
        }

        private void buttonval_Click(object sender, EventArgs e) {
            VariableForm vf = new VariableForm(1, labelval, (int a, string vnamecomp) => {
                labelval.Text = vnamecomp;
            });
            CloseAct.FormOpenCommon(this, vf);
        }
    }
}
