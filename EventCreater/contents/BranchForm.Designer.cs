﻿namespace EventCreater
{
    partial class BranchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBoxActor = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboActorCond = new System.Windows.Forms.ComboBox();
            this.numericUpDownActor = new System.Windows.Forms.NumericUpDown();
            this.comboBoxActorCondSecond = new System.Windows.Forms.ComboBox();
            this.groupBoxVal = new System.Windows.Forms.GroupBox();
            this.radioButton10 = new System.Windows.Forms.RadioButton();
            this.radioButton11 = new System.Windows.Forms.RadioButton();
            this.numericUpDownGroupval = new System.Windows.Forms.NumericUpDown();
            this.labelGroupval = new System.Windows.Forms.Label();
            this.buttonGroupVal = new System.Windows.Forms.Button();
            this.comboBoxGroupValCond = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxCharaDirection = new System.Windows.Forms.ComboBox();
            this.comboBoxActor = new System.Windows.Forms.ComboBox();
            this.comboBoxitemCond = new System.Windows.Forms.ComboBox();
            this.comboBoxitem = new System.Windows.Forms.ComboBox();
            this.comboBoxmon = new System.Windows.Forms.ComboBox();
            this.numericUpDownmon = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxflg = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonval = new System.Windows.Forms.Button();
            this.labelval = new System.Windows.Forms.Label();
            this.buttonflg = new System.Windows.Forms.Button();
            this.labelflg = new System.Windows.Forms.Label();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBoxActor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownActor)).BeginInit();
            this.groupBoxVal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGroupval)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownmon)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBoxActor);
            this.groupBox1.Controls.Add(this.groupBoxVal);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.comboBoxCharaDirection);
            this.groupBox1.Controls.Add(this.comboBoxActor);
            this.groupBox1.Controls.Add(this.comboBoxitemCond);
            this.groupBox1.Controls.Add(this.comboBoxitem);
            this.groupBox1.Controls.Add(this.comboBoxmon);
            this.groupBox1.Controls.Add(this.numericUpDownmon);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.comboBoxflg);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.buttonval);
            this.groupBox1.Controls.Add(this.labelval);
            this.groupBox1.Controls.Add(this.buttonflg);
            this.groupBox1.Controls.Add(this.labelflg);
            this.groupBox1.Controls.Add(this.radioButton6);
            this.groupBox1.Controls.Add(this.radioButton5);
            this.groupBox1.Controls.Add(this.radioButton4);
            this.groupBox1.Controls.Add(this.radioButton3);
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Location = new System.Drawing.Point(12, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(695, 187);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "条件";
            // 
            // groupBoxActor
            // 
            this.groupBoxActor.Controls.Add(this.label2);
            this.groupBoxActor.Controls.Add(this.label1);
            this.groupBoxActor.Controls.Add(this.comboActorCond);
            this.groupBoxActor.Controls.Add(this.numericUpDownActor);
            this.groupBoxActor.Controls.Add(this.comboBoxActorCondSecond);
            this.groupBoxActor.Location = new System.Drawing.Point(347, 41);
            this.groupBoxActor.Name = "groupBoxActor";
            this.groupBoxActor.Size = new System.Drawing.Size(339, 64);
            this.groupBoxActor.TabIndex = 41;
            this.groupBoxActor.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(229, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 43;
            this.label2.Text = "以上";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(229, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 12);
            this.label1.TabIndex = 42;
            this.label1.Text = "を所持、習得、状態";
            // 
            // comboActorCond
            // 
            this.comboActorCond.FormattingEnabled = true;
            this.comboActorCond.Items.AddRange(new object[] {
            "がパーティーにいる。",
            "の名前が",
            "のレベルが",
            "のHPが",
            "がスキル",
            "がアイテム",
            "が状態"});
            this.comboActorCond.Location = new System.Drawing.Point(7, 15);
            this.comboActorCond.Name = "comboActorCond";
            this.comboActorCond.Size = new System.Drawing.Size(105, 20);
            this.comboActorCond.TabIndex = 38;
            this.comboActorCond.SelectedIndexChanged += new System.EventHandler(this.comboActorCond_SelectedIndexChanged);
            // 
            // numericUpDownActor
            // 
            this.numericUpDownActor.Location = new System.Drawing.Point(118, 39);
            this.numericUpDownActor.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDownActor.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownActor.Name = "numericUpDownActor";
            this.numericUpDownActor.Size = new System.Drawing.Size(105, 19);
            this.numericUpDownActor.TabIndex = 40;
            this.numericUpDownActor.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // comboBoxActorCondSecond
            // 
            this.comboBoxActorCondSecond.Enabled = false;
            this.comboBoxActorCondSecond.FormattingEnabled = true;
            this.comboBoxActorCondSecond.Items.AddRange(new object[] {
            "がパーティーにいる。",
            "の名前が",
            "のレベルが",
            "のHPが",
            "がスキル",
            "がアイテム",
            "が状態"});
            this.comboBoxActorCondSecond.Location = new System.Drawing.Point(118, 15);
            this.comboBoxActorCondSecond.Name = "comboBoxActorCondSecond";
            this.comboBoxActorCondSecond.Size = new System.Drawing.Size(105, 20);
            this.comboBoxActorCondSecond.TabIndex = 39;
            // 
            // groupBoxVal
            // 
            this.groupBoxVal.Controls.Add(this.radioButton10);
            this.groupBoxVal.Controls.Add(this.radioButton11);
            this.groupBoxVal.Controls.Add(this.numericUpDownGroupval);
            this.groupBoxVal.Controls.Add(this.labelGroupval);
            this.groupBoxVal.Controls.Add(this.buttonGroupVal);
            this.groupBoxVal.Controls.Add(this.comboBoxGroupValCond);
            this.groupBoxVal.Location = new System.Drawing.Point(81, 58);
            this.groupBoxVal.Name = "groupBoxVal";
            this.groupBoxVal.Size = new System.Drawing.Size(240, 74);
            this.groupBoxVal.TabIndex = 35;
            this.groupBoxVal.TabStop = false;
            // 
            // radioButton10
            // 
            this.radioButton10.AutoSize = true;
            this.radioButton10.Checked = true;
            this.radioButton10.Location = new System.Drawing.Point(1, 9);
            this.radioButton10.Name = "radioButton10";
            this.radioButton10.Size = new System.Drawing.Size(47, 16);
            this.radioButton10.TabIndex = 22;
            this.radioButton10.TabStop = true;
            this.radioButton10.Text = "定数";
            this.radioButton10.UseVisualStyleBackColor = true;
            // 
            // radioButton11
            // 
            this.radioButton11.AutoSize = true;
            this.radioButton11.Location = new System.Drawing.Point(1, 31);
            this.radioButton11.Name = "radioButton11";
            this.radioButton11.Size = new System.Drawing.Size(47, 16);
            this.radioButton11.TabIndex = 23;
            this.radioButton11.Text = "変数";
            this.radioButton11.UseVisualStyleBackColor = true;
            // 
            // numericUpDownGroupval
            // 
            this.numericUpDownGroupval.Location = new System.Drawing.Point(54, 9);
            this.numericUpDownGroupval.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numericUpDownGroupval.Minimum = new decimal(new int[] {
            999999,
            0,
            0,
            -2147483648});
            this.numericUpDownGroupval.Name = "numericUpDownGroupval";
            this.numericUpDownGroupval.Size = new System.Drawing.Size(68, 19);
            this.numericUpDownGroupval.TabIndex = 25;
            // 
            // labelGroupval
            // 
            this.labelGroupval.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelGroupval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelGroupval.Enabled = false;
            this.labelGroupval.Location = new System.Drawing.Point(54, 32);
            this.labelGroupval.Name = "labelGroupval";
            this.labelGroupval.Size = new System.Drawing.Size(147, 16);
            this.labelGroupval.TabIndex = 24;
            this.labelGroupval.Text = "0000:";
            // 
            // buttonGroupVal
            // 
            this.buttonGroupVal.Enabled = false;
            this.buttonGroupVal.Location = new System.Drawing.Point(201, 31);
            this.buttonGroupVal.Name = "buttonGroupVal";
            this.buttonGroupVal.Size = new System.Drawing.Size(37, 18);
            this.buttonGroupVal.TabIndex = 26;
            this.buttonGroupVal.Text = "・・・";
            this.buttonGroupVal.UseVisualStyleBackColor = true;
            this.buttonGroupVal.Click += new System.EventHandler(this.buttonGroupVal_Click);
            // 
            // comboBoxGroupValCond
            // 
            this.comboBoxGroupValCond.FormattingEnabled = true;
            this.comboBoxGroupValCond.Items.AddRange(new object[] {
            "と同値",
            "以上",
            "以下",
            "より大きい",
            "より小さい"});
            this.comboBoxGroupValCond.Location = new System.Drawing.Point(133, 51);
            this.comboBoxGroupValCond.Name = "comboBoxGroupValCond";
            this.comboBoxGroupValCond.Size = new System.Drawing.Size(105, 20);
            this.comboBoxGroupValCond.TabIndex = 27;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(498, 118);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 12);
            this.label5.TabIndex = 34;
            this.label5.Text = "を向いている。";
            // 
            // comboBoxCharaDirection
            // 
            this.comboBoxCharaDirection.FormattingEnabled = true;
            this.comboBoxCharaDirection.Items.AddRange(new object[] {
            "上",
            "右",
            "下",
            "左"});
            this.comboBoxCharaDirection.Location = new System.Drawing.Point(436, 115);
            this.comboBoxCharaDirection.Name = "comboBoxCharaDirection";
            this.comboBoxCharaDirection.Size = new System.Drawing.Size(56, 20);
            this.comboBoxCharaDirection.TabIndex = 33;
            // 
            // comboBoxActor
            // 
            this.comboBoxActor.Enabled = false;
            this.comboBoxActor.FormattingEnabled = true;
            this.comboBoxActor.Items.AddRange(new object[] {
            "アレスタ",
            "エリナ",
            "ペティル"});
            this.comboBoxActor.Location = new System.Drawing.Point(412, 21);
            this.comboBoxActor.Name = "comboBoxActor";
            this.comboBoxActor.Size = new System.Drawing.Size(105, 20);
            this.comboBoxActor.TabIndex = 32;
            // 
            // comboBoxitemCond
            // 
            this.comboBoxitemCond.Enabled = false;
            this.comboBoxitemCond.FormattingEnabled = true;
            this.comboBoxitemCond.Items.AddRange(new object[] {
            "を持っている。",
            "を持っていない。"});
            this.comboBoxitemCond.Location = new System.Drawing.Point(227, 159);
            this.comboBoxitemCond.Name = "comboBoxitemCond";
            this.comboBoxitemCond.Size = new System.Drawing.Size(105, 20);
            this.comboBoxitemCond.TabIndex = 31;
            // 
            // comboBoxitem
            // 
            this.comboBoxitem.Enabled = false;
            this.comboBoxitem.FormattingEnabled = true;
            this.comboBoxitem.Items.AddRange(new object[] {
            "アイテムロード"});
            this.comboBoxitem.Location = new System.Drawing.Point(103, 159);
            this.comboBoxitem.Name = "comboBoxitem";
            this.comboBoxitem.Size = new System.Drawing.Size(105, 20);
            this.comboBoxitem.TabIndex = 30;
            // 
            // comboBoxmon
            // 
            this.comboBoxmon.Enabled = false;
            this.comboBoxmon.FormattingEnabled = true;
            this.comboBoxmon.Items.AddRange(new object[] {
            "と同値",
            "以上",
            "以下",
            "より大きい",
            "より小さい"});
            this.comboBoxmon.Location = new System.Drawing.Point(187, 137);
            this.comboBoxmon.Name = "comboBoxmon";
            this.comboBoxmon.Size = new System.Drawing.Size(105, 20);
            this.comboBoxmon.TabIndex = 29;
            // 
            // numericUpDownmon
            // 
            this.numericUpDownmon.Location = new System.Drawing.Point(103, 138);
            this.numericUpDownmon.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numericUpDownmon.Name = "numericUpDownmon";
            this.numericUpDownmon.Size = new System.Drawing.Size(68, 19);
            this.numericUpDownmon.TabIndex = 28;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(225, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(15, 12);
            this.label4.TabIndex = 21;
            this.label4.Text = "が";
            // 
            // comboBoxflg
            // 
            this.comboBoxflg.FormattingEnabled = true;
            this.comboBoxflg.Items.AddRange(new object[] {
            "ON",
            "OFF"});
            this.comboBoxflg.Location = new System.Drawing.Point(248, 18);
            this.comboBoxflg.Name = "comboBoxflg";
            this.comboBoxflg.Size = new System.Drawing.Size(56, 20);
            this.comboBoxflg.TabIndex = 20;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(225, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 12);
            this.label3.TabIndex = 19;
            this.label3.Text = "が";
            // 
            // buttonval
            // 
            this.buttonval.Enabled = false;
            this.buttonval.Location = new System.Drawing.Point(187, 40);
            this.buttonval.Name = "buttonval";
            this.buttonval.Size = new System.Drawing.Size(32, 18);
            this.buttonval.TabIndex = 18;
            this.buttonval.Text = "・・・";
            this.buttonval.UseVisualStyleBackColor = true;
            this.buttonval.Click += new System.EventHandler(this.buttonval_Click);
            // 
            // labelval
            // 
            this.labelval.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelval.Enabled = false;
            this.labelval.Location = new System.Drawing.Point(81, 41);
            this.labelval.Name = "labelval";
            this.labelval.Size = new System.Drawing.Size(106, 16);
            this.labelval.TabIndex = 17;
            this.labelval.Text = "0000:";
            // 
            // buttonflg
            // 
            this.buttonflg.Enabled = false;
            this.buttonflg.Location = new System.Drawing.Point(187, 18);
            this.buttonflg.Name = "buttonflg";
            this.buttonflg.Size = new System.Drawing.Size(32, 18);
            this.buttonflg.TabIndex = 16;
            this.buttonflg.Text = "・・・";
            this.buttonflg.UseVisualStyleBackColor = true;
            this.buttonflg.Click += new System.EventHandler(this.buttonflg_Click);
            // 
            // labelflg
            // 
            this.labelflg.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelflg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelflg.Enabled = false;
            this.labelflg.Location = new System.Drawing.Point(81, 19);
            this.labelflg.Name = "labelflg";
            this.labelflg.Size = new System.Drawing.Size(106, 16);
            this.labelflg.TabIndex = 15;
            this.labelflg.Text = "0000:";
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Location = new System.Drawing.Point(347, 116);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(83, 16);
            this.radioButton6.TabIndex = 5;
            this.radioButton6.Text = "操作キャラが";
            this.radioButton6.UseVisualStyleBackColor = true;
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(347, 22);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(59, 16);
            this.radioButton5.TabIndex = 4;
            this.radioButton5.Text = "主人公";
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(19, 160);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(60, 16);
            this.radioButton4.TabIndex = 3;
            this.radioButton4.Text = "アイテム";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(19, 138);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(59, 16);
            this.radioButton3.TabIndex = 2;
            this.radioButton3.Text = "所持金";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(19, 41);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(47, 16);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.Text = "変数";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(19, 19);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(48, 16);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "フラグ";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBox1);
            this.groupBox2.Location = new System.Drawing.Point(12, 206);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(695, 39);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "オプション";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(19, 17);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(233, 16);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "条件に当てはまらない処理内容も設定する。";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(189, 262);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 2;
            this.button5.Text = "OK";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(454, 262);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 3;
            this.button6.Text = "キャンセル";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 33;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // BranchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(722, 297);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BranchForm";
            this.Text = "条件分岐";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBoxActor.ResumeLayout(false);
            this.groupBoxActor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownActor)).EndInit();
            this.groupBoxVal.ResumeLayout(false);
            this.groupBoxVal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGroupval)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownmon)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Button buttonflg;
        private System.Windows.Forms.Label labelflg;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxflg;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonval;
        private System.Windows.Forms.Label labelval;
        private System.Windows.Forms.Button buttonGroupVal;
        private System.Windows.Forms.Label labelGroupval;
        private System.Windows.Forms.NumericUpDown numericUpDownGroupval;
        private System.Windows.Forms.RadioButton radioButton11;
        private System.Windows.Forms.RadioButton radioButton10;
        private System.Windows.Forms.ComboBox comboBoxGroupValCond;
        private System.Windows.Forms.NumericUpDown numericUpDownmon;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxCharaDirection;
        private System.Windows.Forms.ComboBox comboBoxActor;
        private System.Windows.Forms.ComboBox comboBoxitemCond;
        private System.Windows.Forms.ComboBox comboBoxitem;
        private System.Windows.Forms.ComboBox comboBoxmon;
        private System.Windows.Forms.GroupBox groupBoxVal;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.ComboBox comboActorCond;
        private System.Windows.Forms.ComboBox comboBoxActorCondSecond;
        private System.Windows.Forms.NumericUpDown numericUpDownActor;
        private System.Windows.Forms.GroupBox groupBoxActor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
    }
}