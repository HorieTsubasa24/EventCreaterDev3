﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventCreater
{
    public static class Variable
    {
        /// <summary>
        /// ファイル名
        /// </summary>
        public static string[] fname = { "flag", "variable", "Status", "CharaView", "CharaViewOut",
                                         "CharaName", "CharaFace", "PlayerVariableParam", "CharaX", "CharaY",
                                         "BattleChara", "Fields", "EventTag"};

        /// <summary>
        /// 変数の名前
        /// </summary>
        public static List<string> variableName = new List<string>();

        /// <summary>
        /// 変数、フラグの初期最大数(ファイルより読み込み)
        /// </summary>
        private const int InitNumVariable = 20;
        
        /// <summary>
        /// <summary>
        /// 変数、フラグの最大数(ファイルより読み込み)
        /// </summary>
        public static int NumVariable;

        /// <summary>
        /// 初期フォーマットテキスト
        /// </summary>
        private static string initStrTemp = "NumVariable:" + InitNumVariable.ToString();
        
        /// <summary>
        /// ファイルがない場合新しく作成。
        /// </summary>
        private static void CreateVariableFile(string fileName)
        {
            FileStream fs = File.Create(AppPath.path + "/Variables/" + fileName + ".dat");
            StreamWriter sw = new StreamWriter(fs);
            sw.WriteLine(initStrTemp);

            for (int i = 0; i < InitNumVariable; i++) {
                sw.WriteLine(i.ToString("D4") + ": ");
            }
            sw.Close();
            fs.Close();
        }
        

        /// <summary>
        /// ファイルを読み込む。変数、フラグファイルの名前を指定する。
        /// </summary>
        /// <param name="fileName"></param>
        public static string[] ReadVariableFile(string fileName)
        {
            if (!File.Exists(AppPath.path + "/Variables/" + fileName + ".dat"))
                CreateVariableFile(fileName);

            FileStream fs = new FileStream(AppPath.path + "/Variables/" + fileName + ".dat", FileMode.Open, FileAccess.Read);
            try
            {
                //抜けると同時に削除
                using (StreamReader sr = new StreamReader(fs))
                {
                    // 最大数記憶
                    string first = sr.ReadLine();
                    var firstar = first.Split(':');
                    NumVariable = int.Parse(firstar[1]);

                    variableName = new List<string>();

                    //一行ごとに読み込める
                    while (!sr.EndOfStream)
                    {
                        // ファイルから一行読み込む
                        var line = sr.ReadLine();

                        // データを分ける
                        var ar = line.Split(':');

                        variableName.Add(ar[1]);
                    }
                }
            }
            catch
            {
                MessageBox.Show("変数、フラグファイルのフォーマットが壊れているかもしれません。\n直してから開いてみてください。",
                                "エラー",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            fs.Close();
            return variableName.ToArray();
        }

        /// <summary>
        /// ファイルのセーブ
        /// 何か変更したら呼び出す。
        /// </summary>
        /// <param name="fileName"></param>
        public static void SaveVariableFile(string fileName)
        {
            List<string> st = new List<string>();
            st.Add("NumVariable:" + NumVariable.ToString());
            st = SetVariableString(st);

            FileStream fs = File.Create(AppPath.path + "/Variables/" + fileName + ".dat");
            StreamWriter sw = new StreamWriter(fs);
            for (int i = 0; i < st.Count; i++)
            {
                sw.WriteLine(st[i]);
            }

            sw.Close();
            fs.Close();
            sw.Dispose();
            fs.Dispose();

            SaveVersion();
        }

        /// <summary>
        /// "{D4}" + 変数名 取得
        /// </summary>
        /// <returns></returns>
        public static string GetVariableLabelText(int n)
        {
            ReadVariableFile(fname[1]);
            return n.ToString("D4") + ":" + Variable.variableName[n];
        }

        /// <summary>
        /// "{D4}" + フラグ名 取得
        /// </summary>
        /// <returns></returns>
        public static string GetFlagLabelText(int n)
        {
            ReadVariableFile(fname[0]);
            return n.ToString("D4") + ":" + Variable.variableName[n];
        }

        /// <summary>
        /// 変数のインデックスを取得
        /// </summary>
        /// <param name="lbl"></param>
        /// <returns></returns>
        public static int GetVariableIndex(Label lbl)
        {
            return int.Parse(lbl.Text.ToString().Substring(0, 4));
        }

        /// <summary>
        /// 変数をList<string>に移す。
        /// これはこのままファイルに出力可能。
        /// </summary>
        /// <param name="st"></param>
        /// <returns></returns>
        public static List<string> SetVariableString(List<string> st)
        {
            for (int i = 0; i < NumVariable; i++)
            {
                st.Add((i).ToString("D4") + ":" + variableName[i]);
            }
            return st;
        }

        /// <summary>
        /// 変数ページリストを作成。
        /// </summary>
        /// <param name="st"></param>
        /// <returns></returns>
        public static List<string> SetVariablePageString(int numofpage)
        {
            List<string> st = new List<string>();
            float numpage = (float)NumVariable / numofpage;
            int n = (int)Math.Ceiling(numpage);
            for (int i = 0; i < n; i++)
            {
                string stmin = (numofpage * i).ToString("D4");
                string stmax = (numofpage * (n - 1) > numofpage * i) ?
                    (numofpage * (1 + i) - 1).ToString("D4") : 
                    (numofpage * (NumVariable / numofpage) + (NumVariable % numofpage) - 1).ToString("D4");
                st.Add(stmin + "-" + stmax);
            }
            return st;
        }

        /// <summary>
        /// 変数の最大数変更
        /// </summary>
        public static void NumVariableChange(int num, string st)
        {
            NumVariable = num;
            if (num > variableName.Count)
            {
                while (num > variableName.Count)
                    variableName.Add(" ");
            }
            else
                variableName.RemoveRange(num, variableName.Count - num);

            SaveVariableFile(st);
        }

        private static void SetComboBoxItem(int id, ComboBox cb)
        {
            cb.Items.Clear();
            cb.Items.AddRange(ReadVariableFile(fname[id]));
            cb.SelectedIndex = 0;
        }

        /// <summary>
        /// ステータス一覧のロード
        /// </summary>
        /// <returns></returns>
        public static string[] StatusLoad()
        {
            return ReadVariableFile(fname[2]);
        }
        /// <summary>
        /// ステータス一覧のロード
        /// </summary>
        /// <returns></returns>
        public static void StatusLoadAndSet(ComboBox cb)
        {
            SetComboBoxItem(2, cb);
            return;
        }
        /// <summary>
        /// キャラ表示方法のロード
        /// </summary>
        /// <param name="cb"></param>
        public static void CharaViewLoadAndSet(ComboBox cb)
        {
            SetComboBoxItem(3, cb);
            return;
        }
        /// <summary>
        /// キャラ表示方法のロード
        /// </summary>
        /// <param name="cb"></param>
        public static void CharaViewOutLoadAndSet(ComboBox cb)
        {
            SetComboBoxItem(4, cb);
            return;
        }
        /// <summary>
        /// キャラ名のロード
        /// </summary>
        /// <param name="cb"></param>
        public static void CharaNameLoadAndSet(ComboBox cb)
        {
            SetComboBoxItem(5, cb);
            return;
        }
        /// <summary>
        /// キャラの顔のロード
        /// </summary>
        /// <param name="cb"></param>
        public static void CharaFaceLoadAndSet(ComboBox cb)
        {
            SetComboBoxItem(6, cb);
            return;
        }

        /// <summary>
        /// 変数の操作のプレイヤーのパラメータロード
        /// </summary>
        /// <param name="cb"></param>
        public static void PlayerVariableParamLoadAndSet(ComboBox cb)
        {
            SetComboBoxItem(7, cb);
            return;
        }

        /// <summary>
        /// キャラの表示のX座標リストをロード
        /// </summary>
        /// <param name="cb"></param>
        public static void CharaXLoadAndSet(ComboBox cb)
        {
            SetComboBoxItem(8, cb);
            return;
        }
        /// <summary>
        /// キャラの表示のY座標リストをロード
        /// </summary>
        /// <param name="cb"></param>
        public static void CharaYLoadAndSet(ComboBox cb) {
            SetComboBoxItem(9, cb);
            return;
        }
        /// <summary>
        /// バトル時の敵グループ名をロード
        /// </summary>
        /// <param name="cb"></param>
        public static void BattleCharaLoadAndSet(ComboBox cb) {
            SetComboBoxItem(10, cb);
            return;
        }
        /// <summary>
        /// バトル時の背景名をロード
        /// </summary>
        /// <param name="cb"></param>
        public static void BattleFieldLoadAndSet(ComboBox cb) {
            SetComboBoxItem(11, cb);
            return;
        }
        /// <summary>
        /// イベントタグのロード
        /// </summary>
        /// <param name="cb"></param>
        public static void EventTagLoadAndSet(ComboBox cb) {
            SetComboBoxItem(12, cb);
            return;
        }

        /// <summary>
        /// バージョン情報をセーブ
        /// </summary>
        public static void SaveVersion() {
            string versionPath = AppPath.path + "/VERSION.txt";
            string text = DateTime.Now.ToString();
            File.WriteAllText(versionPath, text);
        }
    }
}

