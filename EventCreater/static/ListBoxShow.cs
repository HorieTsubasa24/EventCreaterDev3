﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventCreater
{
    public static class ListBoxShow
    {
        /// <summary>
        /// データをリストボックスに追加、表示
        /// </summary>
        public static void ShowDataInListBox(ListBox lb, string[] dat)
        {
            lb.Items.Clear();
            foreach (var a in dat)
                lb.Items.Add(a);
        }
    }
}
