﻿namespace EventCreater
{
    partial class ItemForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxItems = new System.Windows.Forms.ListBox();
            this.listBoxItemBlocks = new System.Windows.Forms.ListBox();
            this.labelVariavle = new System.Windows.Forms.Label();
            this.buttonOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBoxItems
            // 
            this.listBoxItems.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.listBoxItems.FormattingEnabled = true;
            this.listBoxItems.ItemHeight = 15;
            this.listBoxItems.Items.AddRange(new object[] {
            "0001:",
            "0002:",
            "0003:",
            "0004:",
            "0005:",
            "0006:",
            "0007:",
            "0008:",
            "0009:",
            "0010:",
            "0011:",
            "0012:",
            "0013:",
            "0014:",
            "0015:",
            "0016:",
            "0017:",
            "0018:",
            "0019:",
            "0020:"});
            this.listBoxItems.Location = new System.Drawing.Point(171, 27);
            this.listBoxItems.Name = "listBoxItems";
            this.listBoxItems.Size = new System.Drawing.Size(224, 304);
            this.listBoxItems.TabIndex = 5;
            // 
            // listBoxItemBlocks
            // 
            this.listBoxItemBlocks.FormattingEnabled = true;
            this.listBoxItemBlocks.ItemHeight = 12;
            this.listBoxItemBlocks.Items.AddRange(new object[] {
            "0001-0020:"});
            this.listBoxItemBlocks.Location = new System.Drawing.Point(22, 70);
            this.listBoxItemBlocks.Name = "listBoxItemBlocks";
            this.listBoxItemBlocks.ScrollAlwaysVisible = true;
            this.listBoxItemBlocks.Size = new System.Drawing.Size(133, 268);
            this.listBoxItemBlocks.TabIndex = 4;
            this.listBoxItemBlocks.Click += new System.EventHandler(this.listBoxItemBlocks_Click);
            this.listBoxItemBlocks.SelectedValueChanged += new System.EventHandler(this.listBoxItemBlocks_Click);
            // 
            // labelVariavle
            // 
            this.labelVariavle.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelVariavle.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelVariavle.ForeColor = System.Drawing.SystemColors.Control;
            this.labelVariavle.Location = new System.Drawing.Point(22, 27);
            this.labelVariavle.Name = "labelVariavle";
            this.labelVariavle.Size = new System.Drawing.Size(133, 31);
            this.labelVariavle.TabIndex = 3;
            this.labelVariavle.Text = "アイテムリスト";
            this.labelVariavle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(171, 358);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 14;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // ItemForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 393);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.listBoxItems);
            this.Controls.Add(this.listBoxItemBlocks);
            this.Controls.Add(this.labelVariavle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ItemForm";
            this.Text = "アイテムリスト";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxItems;
        private System.Windows.Forms.ListBox listBoxItemBlocks;
        private System.Windows.Forms.Label labelVariavle;
        private System.Windows.Forms.Button buttonOK;
    }
}