﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class ItemForm : Form
    {
        /// <summary>
        /// 一つのページに含まれるアイテムの数。
        /// </summary>
        private const int NumOfPage = 20;

        /// <summary>
        /// アイテムモードとスキルモードに分ける。
        /// </summary>
        private int ModeSkill;

        /// <summary>
        /// アイテム範囲のページ
        /// -1で非選択
        /// </summary>
        private int valPage = 0;

        /// <summary>
        /// 選択しているアイテム
        /// -1で非選択
        /// </summary>
        private int valIndex = 0;
        
        /// <summary>
        /// デリゲードメソッドを入れることでアイテムを選択できる。
        /// </summary>
        /// <param name="mode">0:Item 1:Skill</param>
        public ItemForm(int mode, DoRetValNum del = null)
        {
            InitializeComponent();

            retNum = del;
            ModeSkill = mode;
            if (ModeSkill == 1) this.Name = "スキルリスト";

            Item.ReaditemFile(Item.fname[ModeSkill]);
            ShowItemPageList();
            ShowItemList();
            valPage = listBoxItemBlocks.SelectedIndex = (listBoxItemBlocks.Items.Count != 0) ? 0 : -1;
            valIndex = listBoxItems.SelectedIndex = (listBoxItems.Items.Count != 0) ? 0 : -1;
        }

        public delegate void DoRetValNum(int num, string vname);
        /// <summary>
        /// 値を返すためのコールバック用デリゲード
        /// </summary>
        private DoRetValNum retNum;

        /// <summary>
        /// アイテム群の表示
        /// </summary>
        private void ShowItems()
        {
            ShowItemPageList();
            ShowItemList();
            listBoxItemBlocks.SelectedIndex = valPage;
            listBoxItems.SelectedIndex = valIndex;
        }

        /// <summary>
        /// 有効化時にフォーム内容セット
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ItemForm_EnabledChanged(object sender, EventArgs e)
        {
            if (this.Enabled == true)
            {
                ShowItems();
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
        
        /// <summary>
        /// フォームを閉じる。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// アイテム範囲クリック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBoxItemBlocks_Click(object sender, EventArgs e)
        {
            // SelectedItem = 文字値
            // SelectedIndex = 0 ~ n
            ListBox lb = (ListBox)sender;
            valPage = lb.SelectedIndex;
            ShowItemList(valPage);
        }


        /// <summary>
        /// アイテムページリスト表示
        /// </summary>
        private void ShowItemPageList()
        {
            List<string> st = new List<string>();
            st = Item.SetItemPageString(NumOfPage);
            listBoxItemBlocks.Items.Clear();
            for (int i = 0; i < st.Count; i++)
                listBoxItemBlocks.Items.Add(st[i]);
        }

        /// <summary>
        /// アイテムリスト表示
        /// </summary>
        private void ShowItemList(int page = 0)
        {
            if (page == -1) return;
            List<string> st = new List<string>();
            st = Item.SetItemString(st);
            listBoxItems.Items.Clear();
            for (int n = 0, i = page * NumOfPage; (n < NumOfPage) && (n + valPage * NumOfPage < Item.NumItem); i++, n++)
                listBoxItems.Items.Add(st[valPage * NumOfPage + n]);
        }
        
        /// <summary>
        /// フォーカスが移った時" "だと文字なしにしてから入力させる。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxItemName_Enter(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text == " ")
                tb.Text = "";
        }

        /// <summary>
        /// OKボタンで閉じる
        /// アイテム選択時のときは選択アイテムを拾う。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOK_Click(object sender, EventArgs e)
        {
            // 閉じるときにデリゲードメソッドを実行する。
            if (retNum != null)
            {
                if (listBoxItemBlocks.SelectedIndex == -1)
                    listBoxItemBlocks.SelectedIndex = 0;

                int index = listBoxItems.SelectedIndex;
                int page = listBoxItemBlocks.SelectedIndex;
                int Number = index + page * NumOfPage;
                if (!(index < 0 || Number >= Item.NumItem))
                    retNum.Invoke(Number, Item.GetItemLabelText(Number));
                retNum = null;
            }
            this.Close();
        }
        
    }
}
