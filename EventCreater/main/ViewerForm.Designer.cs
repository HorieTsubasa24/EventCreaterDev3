﻿namespace EventCreater{
    partial class ViewerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.viewer = new System.Windows.Forms.ListBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonOutPut = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // viewer
            // 
            this.viewer.FormattingEnabled = true;
            this.viewer.ItemHeight = 12;
            this.viewer.Location = new System.Drawing.Point(26, 40);
            this.viewer.Name = "viewer";
            this.viewer.Size = new System.Drawing.Size(589, 352);
            this.viewer.TabIndex = 0;
            this.viewer.DoubleClick += new System.EventHandler(this.viewer_DoubleClick);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(288, 408);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 1;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonOutPut
            // 
            this.buttonOutPut.Location = new System.Drawing.Point(26, 11);
            this.buttonOutPut.Name = "buttonOutPut";
            this.buttonOutPut.Size = new System.Drawing.Size(75, 23);
            this.buttonOutPut.TabIndex = 2;
            this.buttonOutPut.Text = "出力";
            this.buttonOutPut.UseVisualStyleBackColor = true;
            this.buttonOutPut.Click += new System.EventHandler(this.buttonOutPut_Click);
            // 
            // ViewerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(646, 443);
            this.Controls.Add(this.buttonOutPut);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.viewer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ViewerForm";
            this.Text = "ViewerForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox viewer;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonOutPut;
    }
}