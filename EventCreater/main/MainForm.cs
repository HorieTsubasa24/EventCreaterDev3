﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class EventList : Form
    {
        /// <summary>
        /// 表示するデータバッファ、イベントのタイトル
        /// </summary>
        private string[] viewBuf;

        /// <summary>
        /// 内部データ用のラベルバッファ
        /// イベントを編集するときに使用。
        /// </summary>
        private string[] labels;

        /// <summary>
        /// 現在の選択しているカーソル値。
        /// EnableChange時の読み込み後にListboxのSelectedIndexになる・
        /// </summary>
        public static int _selectedCursor;

        public EventList()
        {
            AppPath.SetAppPath();
            InitializeComponent();
        }

        /// <summary>
        /// フォームロード時にイベントリストの読み込み
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EventList_Load(object sender, EventArgs e)
        {
            EventData.LoadEventListFormFile();
            ViewEventList();
        }

        /// <summary>
        /// 有効化時に再度ロード
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EventList_EnabledChanged(object sender, EventArgs e)
        {
            var form = (Form)sender;
            if (form.Enabled)
            {
                EventData.LoadEventListFormFile();
                ViewEventList();
                if (_selectedCursor < listBoxMainEvent.Items.Count)
                    listBoxMainEvent.SelectedIndex = _selectedCursor;
            }
        }

        /// <summary>
        /// イベントリストの表示
        /// srcBuf -> viewBuf
        /// </summary>
        private void ViewEventList()
        {
            var ars = EventData.srcBuf[0].Split(':');
            EventData.NumEvent = int.Parse(ars[1]);

            EventData.BufToEventNameAndLabels(ref viewBuf, ref labels);
            ViewBufViewToListBox(viewBuf);
        }

        /// <summary>
        /// リストボックスにイベントを表示
        /// </summary>
        private void ViewBufViewToListBox(string[] dat)
        {
            ListBoxShow.ShowDataInListBox(listBoxMainEvent, dat);
        }

        /// <summary>
        /// イベント項目ダブルクリックでイベント編集
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBoxMainEvent_DoubleClick(object sender, EventArgs e)
        {   /* 参照用
            ListBox lb = (ListBox)sender;
            // SelectedItem = 文字値
            // SelectedIndex = 0 ~ n
            Console.WriteLine(lb.SelectedIndex);
            // mea.Location = マウス座標
            MouseEventArgs mea = (MouseEventArgs)e;
            Console.WriteLine(mea.Location);
            */
            ListBox lb = (ListBox)sender;
            if (lb.SelectedIndex != -1 && lb.SelectedIndex < lb.Items.Count)
            {
                _selectedCursor = lb.SelectedIndex;
                EventForm ef = new EventForm(labels[lb.SelectedIndex]);
                CloseAct.FormOpenCommon(this, ef);
            }
        }

        /// <summary>
        /// イベント作成フォームを開く
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonEventCreater_Click(object sender, EventArgs e)
        {
            _selectedCursor = listBoxMainEvent.Items.Count;
            EventForm ef = new EventForm();
            CloseAct.FormOpenCommon(this, ef);
        }

        /// <summary>
        /// フラグ作成フォームを開く
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonFlagCreater_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(0, null);
            CloseAct.FormOpenCommon(this, vf);
        }

        /// <summary>
        /// 変数作成フォームを開く
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonVariableCreater_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(1, null);
            CloseAct.FormOpenCommon(this, vf);
        }

        /// <summary>
        /// アイテムリストフォームを開く
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonItemCreater_Click(object sender, EventArgs e)
        {
            ItemForm itf = new ItemForm(0);
            CloseAct.FormOpenCommon(this, itf);
        }

        /// <summary>
        /// スキルリストフォームを開く
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSkillList_Click(object sender, EventArgs e)
        {
            ItemForm skf = new ItemForm(1);
            CloseAct.FormOpenCommon(this, skf);
        }
        

        /// <summary>
        /// BGMフォーム
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button5_Click(object sender, EventArgs e)
        {
            Form fm = new SoundForm(0, null, 2);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// 効果音フォーム
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button6_Click(object sender, EventArgs e)
        {
            Form fm = new SoundForm(1, null, 2);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// キャラ名編集追加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(5, null);
            CloseAct.FormOpenCommon(this, vf);
        }

        /// <summary>
        /// ステータス以上編集
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e) {
            VariableForm vf = new VariableForm(2, null);
            CloseAct.FormOpenCommon(this, vf);
        }

        /// <summary>
        /// イベントタグ編集
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonEventTag_Click(object sender, EventArgs e) {
            VariableForm vf = new VariableForm(12, null);
            CloseAct.FormOpenCommon(this, vf);
        }

        /// <summary>
        /// 任意のファイルに書き出し
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSaveToFile_Click(object sender, EventArgs e) {
            // データのキャッシュ
            var data = DirectoryUtils.ReadFile("Events", "List");

            // セーブダイアログ
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.FileName = "EVH_.dat";
            sfd.InitialDirectory = AppPath.path + "/Events"; // バグってる
            sfd.Title = "List.datの書き出し";
            //ダイアログボックスを閉じる前に現在のディレクトリを復元するようにする
            sfd.RestoreDirectory = true;

            if (sfd.ShowDialog() == DialogResult.OK) {
                DirectoryUtils.SaveFile(sfd.FileName, data);
            }
        }

        /// <summary>
        /// Viewer出力のタイトル部のヘッダを取得する。
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private string GetEventTitle(string str) {
            string title = null;
            // イベントタイトル
            int HEADLN = 7;
            if (str.Length >= HEADLN && str.IndexOf("EV") == 0 && str.IndexOf(":") == HEADLN - 1) {
                int evNO = -1;
                if (int.TryParse(str.Substring(2, 4), out evNO)) {
                    title = "EV" + evNO.ToString("D4") + ":" + str.Substring(HEADLN, str.Length - HEADLN);
                }
            }
            return title;
        }

        /// <summary>
        /// Viewerを表示する。
        /// </summary>
        /// <param name="outcaches"></param>
        /// <param name="formText"></param>
        private void ShowViewerForm(IEnumerable<string> outcaches, string formText) {
            ViewerForm vf = new ViewerForm(outcaches, this);
            vf.Text = formText + "抽出";
            vf.Show();
        }

        /// <summary>
        /// イベント内のTODOを出力する
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonTODO_Click(object sender, EventArgs e) {
            const string TODO = "TODO";
            var data = DirectoryUtils.ReadFile("Events", "List");
            string title = "";
            var outcaches = new List<string>();
            foreach (var str in data) {
                string titleRes = GetEventTitle(str);
                if (!string.IsNullOrEmpty(titleRes)) {
                    title = titleRes;
                }

                if (str.Contains(TODO)) {
                    outcaches.Add(title);
                    outcaches.Add(str);
                }
            }

            ShowViewerForm(outcaches, TODO);
        }

        /// <summary>
        /// イベント内のサウンド関係イベントを出力する
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonFindSound_Click(object sender, EventArgs e) {
            const string BGM = "PlayBGM(";
            const string SE = "PlaySE(";
            var data = DirectoryUtils.ReadFile("Events", "List");
            string title = "";
            var outcaches = new List<string>();
            foreach (var str in data) {
                string titleRes = GetEventTitle(str);
                if (!string.IsNullOrEmpty(titleRes)) {
                    title = titleRes;
                }

                if (str.Contains(BGM)) {
                    outcaches.Add(title);
                    outcaches.Add(str);
                } else if (str.Contains(SE)) {
                    outcaches.Add(title);
                    outcaches.Add(str);
                }
            }

            ShowViewerForm(outcaches, BGM + ", " + SE);
        }

        /// <summary>
        /// FTPリクエストを得る
        /// </summary>
        /// <param name="localPath"></param>
        /// <param name="remotePath"></param>
        /// <returns></returns>
        private System.Net.FtpWebRequest ConfigurateFTP(string remotePath,string method, bool keepAlive) {
            // ダウンロードするファイルのURI
            Uri uri = new Uri(remotePath);
            //FtpWebReqestの作成
            System.Net.FtpWebRequest ftpReq = (System.Net.FtpWebRequest)System.Net.FtpWebRequest.Create(uri);
            ftpReq.Method = method;
            // ログインユーザー名　パスワード
            ftpReq.Credentials = new System.Net.NetworkCredential("terainast.html.xdomain.jp", "Passtsu0");
            //要求の完了後に接続したままにするか
            ftpReq.KeepAlive = keepAlive;
            //バイナリモードで転送する
            ftpReq.UseBinary = true;
            //PASSIVEモードを無効にする
            ftpReq.UsePassive = false;

            return ftpReq;
        }

        /// <summary>
        /// ストリームのデータをファイルに書き込む
        /// </summary>
        /// <param name="strm"></param>
        /// <param name="fs"></param>
        private void StreamToFile(Stream strm, FileStream fs) {
            byte[] buffer = new byte[1024];
            while (true) {
                int readSize = strm.Read(buffer, 0, buffer.Length);
                if (readSize == 0)
                    break;
                fs.Write(buffer, 0, readSize);
            }
        }
        /// <summary>
        /// ファイルのデータをストリームに書き込む
        /// </summary>
        /// <param name="strm"></param>
        /// <param name="fs"></param>
        private void FileToStream(FileStream fs, Stream strm) {
            byte[] buffer = new byte[1024];
            while (true) {
                int readSize = fs.Read(buffer, 0, buffer.Length);
                if (readSize == 0)
                    break;
                strm.Write(buffer, 0, readSize);
            }
        }

        /// <summary>
        /// ローカルのファイルをリモートのファイルに更新する
        /// </summary>
        /// <param name="localPath"></param>
        /// <param name="remotePath"></param>
        private void RemoteToLocalFile(string remotePath, string localPath, bool keepConecting = true) {
            Console.WriteLine("pull {0} TO {1}", remotePath, localPath);

            // 保存先
            string downFile = localPath;

            // FtpWebRequestを取得
            System.Net.FtpWebResponse ftpRes = null;
            Stream resStrm = null;
            FileStream fs = null;
            try {
                ftpRes = (System.Net.FtpWebResponse)ConfigurateFTP(remotePath, System.Net.WebRequestMethods.Ftp.DownloadFile, keepConecting).GetResponse();
                // ファイルをダウンロードするためのStreamを取得
                resStrm = ftpRes.GetResponseStream();
                // ダウンロードしたファイルを書き込むためのFileStream
                fs = new FileStream(downFile, FileMode.Create, FileAccess.Write);

                StreamToFile(resStrm, fs);
                //FTPサーバーから送信されたステータスを表示
                Console.WriteLine("{0}: {1}", ftpRes.StatusCode, ftpRes.StatusDescription);
            } catch {
                fs?.Close();
                resStrm?.Close();
                ftpRes?.Close();
                throw;
            }
            //閉じる
            fs?.Close();
            resStrm?.Close();
            ftpRes?.Close();
        }

        /// <summary>
        /// リモートのファイルを削除する
        /// </summary>
        private void DeleteRemoteFile(string remotePath) {
            System.Net.FtpWebResponse ftpRes = null;
            try { 
                System.Net.FtpWebRequest ftpReq = ConfigurateFTP(remotePath, System.Net.WebRequestMethods.Ftp.DeleteFile, true);
                ftpRes = (System.Net.FtpWebResponse)ftpReq.GetResponse();
                //FTPサーバーから送信されたステータスを表示
                Console.WriteLine("{0}: {1}", ftpRes.StatusCode, ftpRes.StatusDescription);
            } catch (System.Net.WebException) {
                ftpRes?.Close();
                ftpRes = null;
            } catch {
                ftpRes?.Close();
                throw;
            }
            //閉じる
            ftpRes?.Close();
        }

        /// <summary>
        /// リモートのファイルをローカルのファイルに置き換える
        /// </summary>
        /// <param name="localPath"></param>
        /// <param name="remotePath"></param>
        private void LocalToRemoteFile(string localPath, string remotePath, bool keepConecting = true) {
            Console.WriteLine("push {0} TO {1}", localPath, remotePath);

            //DeleteRemoteFile(remotePath);
            System.Net.FtpWebResponse ftpRes = null;
            Stream ftpStrm = null;
            FileStream fs = null;
            try {
                System.Net.FtpWebRequest ftpReq = ConfigurateFTP(remotePath, System.Net.WebRequestMethods.Ftp.UploadFile, keepConecting);
                ftpRes = (System.Net.FtpWebResponse)ftpReq.GetResponse();
                ftpStrm = ftpReq.GetRequestStream();
                fs = new FileStream(localPath, FileMode.Open, FileAccess.Read);
                FileToStream(fs, ftpStrm);
                //FTPサーバーから送信されたステータスを表示
                Console.WriteLine("{0}: {1}", ftpRes.StatusCode, ftpRes.StatusDescription);
                //閉じる
            } catch {
                fs?.Close();
                ftpStrm?.Close();
                ftpRes?.Close();
                throw;
            }
            fs?.Close();
            ftpStrm?.Close();
            ftpRes?.Close();
        }

        /// <summary>
        /// ファイル転送のプログレス表示
        /// </summary>
        private void ShowProgress(out Form form, out ProgressBar progress, int length, bool isPush) {
            form = new Form();
            form.Size = new System.Drawing.Size(180, 100);
            form.Text = isPush == true ? "Push" : "Pull";
            form.ControlBox = false;
            progress = new ProgressBar();
            progress.Location = new System.Drawing.Point(40, 20);
            progress.Minimum = 1;
            progress.Maximum = length - 1;
            progress.Value = 1;
            progress.Parent = form;
        }

        /// <summary>
        /// サーバーとの接続を切断する。
        /// </summary>
        private void CloseNetConnection() {
            System.Net.FtpWebRequest req = (System.Net.FtpWebRequest)System.Net.FtpWebRequest.Create("ftp://terainast.html.xdomain.jp/VERSION.txt");
            req.Method = System.Net.WebRequestMethods.Ftp.GetDateTimestamp;
            req.Credentials = new System.Net.NetworkCredential("terainast.html.xdomain.jp", "Passtsu0");
            req.KeepAlive = false;
            var res = (System.Net.FtpWebResponse)req.GetResponse();
            Console.WriteLine("{0}: {1}", res.StatusCode, res.StatusDescription);
        }

        /// <summary>
        /// 接続転送
        /// </summary>
        /// <param name="LocalToRemote"></param>
        /// <param name="RemoteToLocal"></param>
        private void TransferProcess(bool LocalToRemote, bool RemoteToLocal) {
            string versionPath = AppPath.path + "/VERSION.txt";
            string versionRemotePath = "ftp://terainast.html.xdomain.jp/VERSION.txt";

            if (LocalToRemote || RemoteToLocal == false) {
                System.Net.FtpWebResponse ftpRes = null;
                Stream strm = null;
                FileStream fs = null;
                // バージョン情報を得て、こちらのほうが新しければアップロードする
                try {
                    if (File.Exists(versionPath) == false) {
                        string date = DateTime.Now.ToString();
                        File.WriteAllText(versionPath, date);
                        throw new Exception("ローカルのバージョン情報なし ");
                    }

                    System.Net.FtpWebRequest ftpReq = ConfigurateFTP(versionRemotePath, System.Net.WebRequestMethods.Ftp.DownloadFile, true);
                    ftpRes = (System.Net.FtpWebResponse)ftpReq.GetResponse();
                    strm = ftpRes.GetResponseStream();
                    fs = new FileStream(versionPath + "tmp", FileMode.Create, FileAccess.Write);
                    StreamToFile(strm, fs);
                } catch (System.Net.WebException) {
                    // リモートにファイルがない場合、こちらのほうが新しいとする
                    LocalToRemote = true;
                    RemoteToLocal = false;
                }
                fs?.Close();
                strm?.Close();
                ftpRes?.Close();

                // ファイル先頭にある日付時間を現在のDateTimeと比較
                if (LocalToRemote == false) {
                    string strRemoteDateTime = File.ReadAllLines(versionPath + "tmp")[0];
                    DateTime versionRemoteDate = DateTime.Parse(strRemoteDateTime);

                    string strLocalDateTime = File.ReadAllLines(versionPath)[0];
                    DateTime versionLocalDate = DateTime.Parse(strLocalDateTime);

                    if (versionLocalDate > versionRemoteDate)
                        LocalToRemote = true;
                    else
                        RemoteToLocal = true;
                }
            }

            string[] fileList = File.ReadAllLines(AppPath.path + "/TableUpdateList.csv");
            Form form;
            ProgressBar progress;
            ShowProgress(out form, out progress, fileList.Length, LocalToRemote);
            CloseAct.FormOpenCommon(this, form);
            progress.Show();
            for (int i = 1; i < fileList.Length; i++) {
                progress.Value = i;

                string[] vs = fileList[i].Split(',');
                string localPath = AppPath.path + vs[0];
                string remotePath = "ftp://terainast.html.xdomain.jp" + vs[1];
                bool canLocalToRemote = vs[2] == "OK";
                bool canRemoteToLocal = vs[3] == "OK";

                // Remote -> Local
                if (RemoteToLocal == true && canRemoteToLocal) {
                    RemoteToLocalFile(remotePath, localPath);
                }

                // Local -> Remote
                if (LocalToRemote == true && canLocalToRemote) {
                    LocalToRemoteFile(localPath, remotePath);
                }
            }

            // バージョンを更新
            if (LocalToRemote == true) {
                string date = DateTime.Now.ToString();
                File.WriteAllText(versionPath, date);
                LocalToRemoteFile(versionPath, versionRemotePath, true);
            } else if (RemoteToLocal == true) {
                RemoteToLocalFile(versionRemotePath, versionPath + "tmp", true);
                string date = File.ReadAllLines(versionPath + "tmp")[0];
                File.WriteAllText(versionPath, date);
            }

            if (File.Exists(versionPath + "tmp")) {
                File.Delete(versionPath + "tmp");
            }

            progress.Dispose();
            form.Close();
            form.Dispose();

            CloseNetConnection();

            EventData.LoadEventListFormFile();
            ViewEventList();
        }

        /// <summary>
        /// テーブルをダウンロードして更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpdateTable_Click(object sender, EventArgs e) {
            TransferProcess(false, false);
        }
        
        /// <summary>
        /// リモートから取得
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRemoteToLocal_Click(object sender, EventArgs e) {
            TransferProcess(false, true);
        }

        /// <summary>
        /// ローカルからPUSH
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonLocalToRemote_Click(object sender, EventArgs e) {
            TransferProcess(true, false);
        }

    }
}
