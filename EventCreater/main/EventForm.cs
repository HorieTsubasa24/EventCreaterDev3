﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class EventForm : Form
    {
        private List<string> Codes { get { return EventData.evCont.codes; } set { EventData.evCont.codes = value; } }
        private ListBox.SelectedIndexCollection ECSelIds { get { return EventContents.SelectedIndices; } }
        private ListBox.ObjectCollection Items { get { return EventContents.Items; } }

        bool IsLoadComp = false;

        /// <summary>
        /// 外部から参照するときに使う。
        /// </summary>
        public static EventForm eventForm;
        
        /// <summary>
        /// イベントデータでstrsのセーブに使う。
        /// </summary>
        public ListBox EVConts { get { return EventContents; } set { EventContents = value; } }


        public EventForm(string labelname = null)
        {
            InitializeComponent();
            if (labelname != null)
                this.Text = "イベント:" + labelname;
            eventForm = this;

            // イベントタグロード
            Variable.EventTagLoadAndSet(comboBoxEventTag);

            EventData.ResetEventData();
            FormDataInit();
            EventData.AppStrs = new List<string>();
            EventData.EvcStrs = new List<string>();
            LoadEventContents(labelname);
            IsLoadComp = true;
            SetFormParamToConditionFromCode();
            SetCodeToContentsFromCode();
        }

        /// <summary>
        /// フォームの初期化
        /// </summary>
        private void FormDataInit()
        {
            Items.Clear();
            Items.Add("◆");
            SetInitParam();
            SetConditionToForm();
        }

        /// <summary>
        /// 変数を扱うときは直前に呼び出す。
        /// </summary>
        private void VariableListLoad()
        {
            Variable.ReadVariableFile(Variable.fname[1]);
        }

        /// <summary>
        /// フラグを扱うときは直前に呼び出す。
        /// </summary>
        private void FlagListLoad()
        {
            Variable.ReadVariableFile(Variable.fname[0]);
        }

        /// <summary>
        /// 初期パラメータセット
        /// </summary>
        private void SetInitParam()
        {
            EventData.EventID = EventData.GetNewEventID();
            EventData.apCond = new AppearCondition(0, 0, false, 0, false, false, 0, 0, AppearCondition.Condition.Eq, false, 0, 0, 0, 0);
        }

        /// <summary>
        /// イベントダブルクリックで開いた時、
        /// labelname != null イベント内容読み込み
        /// </summary>
        /// <param name="labelname"></param>
        private void LoadEventContents(string labelname)
        {
            if (labelname == null) return;
            EventLoad(labelname);

            SetConditionToForm(EventData.apCond.flagNo, EventData.apCond.valNo);
        }

        /// <summary>
        /// パラメータをフォームにセット
        /// </summary>
        private void SetConditionToForm(int fn = 0, int vf = 0)
        {
            int playcond = (EventData.apCond.playCond != -1) ? EventData.apCond.playCond : 0;
            comboBoxPlayCond.SelectedIndex = playcond;
            comboBoxEventTag.SelectedIndex = EventData.apCond.eventTag;
            textBoxEventName.Text = EventData.apCond.eventName;
            checkBoxFlag.Checked = EventData.apCond.checkFlag;
            comboBoxFlagCondition.SelectedIndex = Convert.ToInt32(EventData.apCond.isFlagCondTrue);
            checkBoxVariable.Checked = EventData.apCond.checkVariable;
            comboBoxVariableCondition.SelectedIndex = (int)EventData.apCond.condition;
            numericUpDownVariable.Value = EventData.apCond.valLimit;
            checkBoxMember.Checked = EventData.apCond.checkMen;
            comboBoxMember.SelectedIndex = EventData.apCond.menNo;
            numericUpDownFloor.Value = EventData.apCond.Floor;
            numericUpDownX.Value = EventData.apCond.PosX;
            numericUpDownY.Value = EventData.apCond.PosY;

            FlagListLoad();
            labelFlag.Text = Variable.GetFlagLabelText(fn);
            VariableListLoad();
            labelVariable.Text = Variable.GetVariableLabelText(vf);
        }

        /// <summary>
        /// パラメーター変更時、エンターキー押下時、フォーカスが外れる時、
        /// コンディションフォーム内容をコンディションデータに保存
        /// </summary>
        private void SetFormParamToCondition(object sender, EventArgs e)
        {
            SetFormParamToConditionFromCode();
        }

        /// <summary>
        /// フォームの出現条件をキャッシュする。
        /// </summary>
        private void SetFormParamToConditionFromCode()
        {
            if (!IsLoadComp) return;
            int playcond = (comboBoxPlayCond.SelectedIndex != -1) ? comboBoxPlayCond.SelectedIndex : 0;
            int eventtag = comboBoxEventTag.SelectedIndex;
            int flagno = int.Parse((labelFlag.Text).Substring(0, 4));
            int valno = int.Parse((labelVariable.Text).Substring(0, 4));
            int vallim = (int)numericUpDownVariable.Value;
            bool isflagcondtrue = (comboBoxFlagCondition.SelectedIndex == 1);
            AppearCondition.Condition cond = (AppearCondition.Condition)comboBoxVariableCondition.SelectedIndex;
            EventData.apCond = new AppearCondition(playcond, eventtag,
                                         checkBoxFlag.Checked, flagno, isflagcondtrue,
                                         checkBoxVariable.Checked, valno, vallim, cond,
                                         checkBoxMember.Checked, comboBoxMember.SelectedIndex,
                                         (int)numericUpDownFloor.Value, (int)numericUpDownX.Value, (int)numericUpDownY.Value, 
                                         textBoxEventName.Text);
        }

        /// <summary>
        /// イベントコンテンツリストからイベントを設定した時
        /// 呼び出す。(EventDataから)
        /// </summary>
        public void EventContentsSet(string[] code, int isedit)
        {
            string inst = InstructionAnalysis.SeachWordAsString(Codes[ECSelIds[0]]);
            if (isedit == 1)
            {
                if (inst == "If" || inst == "Choice")
                {
                    EventContentsEditRewrite(inst, code);
                    return;
                }
                else
                    DeleteLine();
            }

            int nowidx = 0;
            for (int i = code.Length - 1; i >= 0; i--)
            {
                List<string> dats = Codes;

                Console.Write(dats.Count + "->");

                nowidx = ECSelIds[0];
                code[i] = "\t" + code[i];
                dats.Insert(nowidx, code[i]);

                Console.WriteLine(dats.Count);
            }
            SetCodeToContentsFromCode();
            EventContents.SelectedIndex = nowidx;
            DeterminationOfInstructionGroup();
        }
        
        /// <summary>
        /// If, Choice文の編集。
        /// </summary>
        /// <param name="word"></param>
        private void EventContentsEditRewrite(string word, string[] codes)
        {
            int nowidx = ECSelIds[0];
            int stck = GetLineStack(ECSelIds[0]);
            switch (word)
            {
                case "If":
                    int oelse = int.Parse(Codes[ECSelIds[0]][Codes[ECSelIds[0]].Length - 2].ToString());

                    // 1行目書き換え
                    Codes[ECSelIds[0]] = codes[0];
                    int nelse = int.Parse(Codes[ECSelIds[0]][Codes[ECSelIds[0]].Length - 2].ToString());
                    // Else以降を追加削除
                    if (oelse == 0 && nelse == 1)
                    {
                        string[] inscode = { "Else:", "" };
                        Codes.InsertRange(ECSelIds[ECSelIds.Count - 1], inscode);
                    }
                    else if (oelse == 1 && nelse == 0)
                    {
                        SeachElseAndDel("Else:", "Endif:", nowidx, stck);
                    }
                    break;
                case "Choice":
                    // 引数取得(old)
                    InstructionAnalysis.OpenEditForm(Codes[ECSelIds[0]], 1);
                    string[] owords = (InstructionAnalysis.BefStrs.Length > 0) ? 
                        InstructionAnalysis.BefStrs.Split('/') : new string[1];
                    int ocancel = (InstructionAnalysis.BefArgs[0] == 5) ? 1 : 0;
                    // 引数取得(new)
                    InstructionAnalysis.OpenEditForm(codes[0], 1);
                    string[] nwords = (InstructionAnalysis.BefStrs.Length > 0) ?
                        InstructionAnalysis.BefStrs.Split('/') : new string[1];
                    int ncancel = (InstructionAnalysis.BefArgs[0] == 5) ? 1 : 0;

                    // 1行目書き換え
                    Codes[ECSelIds[0]] = codes[0];

                    // (キャンセルボタンで)独立して分岐 追加削除
                    string[] inscancel = { "Branch(4)", "" };
                    if (ocancel == 0 && ncancel == 1)
                        Codes.InsertRange(ECSelIds[ECSelIds.Count - 1], inscancel);
                    if (ocancel == 1 && ncancel == 0)
                        SeachElseAndDel("Branch(4)", "EndChoice:", nowidx, stck);

                    // Branch("", n)を追加削除
                    string word1 = "Branch(\"", word2 = "\",", word3 = ")";

                    int i = 0;
                    for (i = 0; i < owords.Length; i++)
                    {
                        if (i < nwords.Length)
                        {
                            // oldとnewの範囲内なら書き換える。
                            SeachBranchRewrite(word1 + owords[i] + word2 + i + word3, word1 + nwords[i] + word2 + i + word3);
                        }
                        else if (i >= nwords.Length)
                        {
                            SetCodeToContentsFromCode();
                            EventContents.SelectedIndex = nowidx;
                            DeterminationOfInstructionGroup();

                            // oldから要素が減らされていたら削除する。
                            string seachwd = word1 + owords[i] + word2 + i + word3;
                            string endseach = (ncancel == 1) ? "Branch(4)" : "EndChoice:";
                            SeachElseAndDel(seachwd, endseach, nowidx, stck);
                            break;
                        }
                    }

                    // oldから要素が追加されていたら新しく追加する。
                    for (; i < nwords.Length; i++)
                    {
                        SetCodeToContentsFromCode();
                        EventContents.SelectedIndex = nowidx;
                        DeterminationOfInstructionGroup();

                        string[] insertwords = { word1 + nwords[i] + word2 + i + word3, "" };
                        string insertinst = (ncancel == 1) ? "Branch(4)" : "EndChoice:";
                        int idx = SeachIdxInSelected(insertinst, nowidx, stck);
                        Codes.InsertRange(idx, insertwords);
                    }
                    break;
                    
            }
            SetCodeToContentsFromCode();
            EventContents.SelectedIndex = nowidx;
            DeterminationOfInstructionGroup();
        }

        /// <summary>
        /// s1の行をwordで書き換える。
        /// 返り値に次のインデックス。
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="str"></param>
        private void SeachBranchRewrite(string s1, string word)
        {
            int num = SeachIdxInSelected(s1);
            if (num != -1)
                Codes[num] = word;
        }

        /// <summary>
        /// s1からs2の一つ前の要素を削除
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        private void SeachElseAndDel(string s1, string s2, int nowidx, int stck)
        {
            int num1 = SeachIdxInSelected(s1, nowidx, stck);
            int num2 = SeachIdxInSelected(s2, num1, stck);
            if (num1 == -1)
                return;
            
            int cnt = num1;
            while (cnt < num2)
            {
                Codes.RemoveAt(num1);
                cnt++;
            }
            return;
        }

        
        /// <summary>
        /// 選択範囲に指定のコードが有るか判定。
        /// あればインデックスを返す。ないと-1。
        /// コードのスタックがずれていたら判定しない。
        /// </summary>
        private int SeachIdxInSelected(string word, int startIdx = 0, int startStck = 0)
        {
            if (startIdx == 0)
                startIdx = ECSelIds[0];

            int b_stck = startStck;
            int stck = GetLineStack(startIdx);

            for (int i = startIdx; i <= ECSelIds[ECSelIds.Count - 1]; i++)
            {
                int num = i;
                if (num >= Codes.Count)
                    return -1;

                stck = GetLineStack(num);

                string str = CollectionTool.RemoveChar(Codes[num], '\t');
                if (stck == b_stck && str == word)
                    return i;
                
            }
            return -1;
        }

        /// <summary>
        /// Codeの命令の深さを取得。
        /// </summary>
        /// <param name="startIdx"></param>
        /// <returns></returns>
        private int GetLineStack(int num)
        {
            int stck = 0;
            string s = (string)Items[num];
            for (int j = 0; j < s.Length; j++)
            {
                if (s[j] == '　')
                    stck++;
                else
                    return stck;
            }
            return stck;
        }

        /// <summary>
        /// イベント追加時、変更時、削除時、呼び出す。
        /// EventData.evCont.codeの内容を読み取ってcode->(変換)->strs->listboxとコピー
        /// </summary>
        public void SetCodeToContentsFromCode()
        {
            if (!IsLoadComp) return;
            //TODO:本当は変換処理が必要
            EventData.CodeToStrs();
            EventData.StrsToForm();
        }
        
        /// <summary>
        /// 条件フラグの有/無効果　表示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxFlag_CheckedChanged(object sender, EventArgs e)
        {
            labelFlag.Enabled = checkBoxFlag.Checked;
            buttonFlag.Enabled = checkBoxFlag.Checked;
            comboBoxFlagCondition.Enabled = checkBoxFlag.Checked;
            // 保存
            SetFormParamToConditionFromCode();
        }

        /// <summary>
        /// 条件変数の有/無効果　表示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxVariable_CheckedChanged(object sender, EventArgs e)
        {
            labelVariable.Enabled = checkBoxVariable.Checked;
            buttonVariable.Enabled = checkBoxVariable.Checked;
            numericUpDownVariable.Enabled = checkBoxVariable.Checked;
            comboBoxVariableCondition.Enabled = checkBoxVariable.Checked;
            // 保存
            SetFormParamToConditionFromCode();
        }

        /// <summary>
        /// 条件メンバーの有/無効果　表示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxMember_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxMember.Enabled = checkBoxMember.Checked;
            SetFormParamToConditionFromCode();
        }

        /// <summary>
        /// キャンセルボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 適用ボタンクリック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ApplyButton_Click(object sender, EventArgs e)
        {
            EventSave();
        }

        /// <summary>
        /// OKボタンクリック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OKButton_Click(object sender, EventArgs e)
        {
            EventSave();
            this.Close();
        }

        /// <summary>
        /// イベントのロード
        /// </summary>
        private void EventLoad(string lblname)
        {
            EventData.EventID = int.Parse(lblname.Substring(2, 4));
            List<string> strs = new List<string>();
            strs = DirectoryUtils.ReadFile("Events", "List");
            EventData.NumEvent = CollectionTool.GetEventNum(strs);

            EventData.AppStrs = CollectionTool.GetAppearCondData(EventData.NumEvent, strs.ToArray(), lblname);
            EventData.EvcStrs = CollectionTool.GetEventContentsData(EventData.NumEvent, strs.ToArray(), lblname);
            if (EventData.AppStrs == null || EventData.EvcStrs == null)
                return;

            EventData.apCond.eventName = CollectionTool.GetEventTitle(EventData.NumEvent, strs.ToArray(), lblname);
            SetAppearConditonFromData(EventData.AppStrs);
            SetEventContentsFormData(EventData.EvcStrs);
        }

        /// <summary>
        /// 出現データから条件設定
        /// </summary>
        private void SetAppearConditonFromData(List<string> dat)
        {
            string[] word = { "playcond", "flag", "val", "menm", "position", "eventtag" };
            for (int i = 0; i < dat.Count; i++)
            {
                for (int j = 0; j < word.Length; j++)
                {
                    if (dat[i].IndexOf("//") != -1)
                    {
                        j = word.Length;
                        continue;
                    }
                    int index = dat[i].IndexOf(word[j]);
                    if (index != -1)
                    {
                        string argument = dat[i].Split('(')[1];
                        argument = argument.Substring(0, argument.Length - 1);
                        SetAppearCond(j, argument);
                    }
                }
            }
        }

        /// <summary>
        /// イベントデータからcode, strs, listBoxにセット。
        /// </summary>
        private void SetEventContentsFormData(List<string> dat)
        {
            // ロードしている時、new -> add("\t")で作られたものは必要ないのでnewしちゃう
            Codes = new List<string>();
            Codes.AddRange(dat.ToArray());
        }

        /// <summary>
        /// 出現変数セット
        /// </summary>
        /// <param name="like"></param>
        /// <param name="arg"></param>
        private void SetAppearCond(int like, string arg)
        {
            string[] strs = arg.Split(',');
            int[] args = { 0, 0, 0, 0, 0, 0, 0 };
            for (int i = 0; i < strs.Length; i++)
                args[i] = int.Parse(strs[i]);

            switch(like)
            {
                case 0:     //playcond
                    EventData.apCond.PlayCondSet(args[0]);
                    break;
                case 1:     //flag
                    EventData.apCond.FlagSet(args[0], args[1], args[2]);
                    break;
                case 2:     //val
                    EventData.apCond.ValSet(args[0], args[1], args[2], args[3]);
                    break;
                case 3:     //menm
                    EventData.apCond.MenmSet(args[0], args[1]);
                    break;
                case 4:     //position
                    EventData.apCond.PositionSet(args[0], args[1], args[2]);
                    break;
                case 5:     //eventTag
                    EventData.apCond.EventTagSet(args[0]);
                    break;
                default:
                    throw new Exception("想定外の出現条件です。");
            }
        }

        /// <summary>
        /// イベントの保存
        /// </summary>
        private void EventSave()
        {
            SetFormParamToConditionFromCode();
            SetCodeToContentsFromCode();
            EventData.SaveEventData();
        }
        
        /// <summary>
        /// イベントの選択処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EventContents_DoubleClick(object sender, EventArgs e)
        {
            var a = (MouseEventArgs)e;

            // マウス座標から選択すべきアイテムのインデックスを取得
            int idx = EventContents.IndexFromPoint(a.Location);
            
            // すべての選択状態を解除してから
            //EventContents.ClearSelected();

            // アイテムを選択
            EventContents.SelectedIndex = idx;
            // インデックスが取得できたら
            if (idx >= 0)
            {
                string st = Items[idx].ToString();
                // 全角スペース削除
                while(st[0] == '　')
                {
                    st = st.Remove(0, 1);
                }

                // 接頭記号が＿のときはなにもしない。
                if (st[0] == '＿')
                    return;

                // イベント選択フォームを開く
                OpenEventSelectForm();
            }
        }

        /// <summary>
        /// イベントコンテンツを選択するフォームを開く
        /// </summary>
        private void OpenEventSelectForm()
        {
            Form fm = new EventActListForm();
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// 変数リスト表示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonVariableCreater_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(1, null);
            CloseAct.FormOpenCommon(this, vf);
        }

        /// <summary>
        /// フラグリスト表示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonFlagCreater_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(0, null);
            CloseAct.FormOpenCommon(this, vf);
        }

        /// <summary>
        /// 条件フラグ選択フォームを開く
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonFlag_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(0, labelFlag, (int a, string fnamecomp) => 
            {
                EventData.apCond.flagNo = a;
                labelFlag.Text = fnamecomp;
            });
            CloseAct.FormOpenCommon(this, vf);
        }

        /// <summary>
        /// 条件変数選択フォームを開く
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonVariable_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(1, labelVariable, (int a, string vnamecomp) =>
            {
                EventData.apCond.valNo = a;
                labelVariable.Text = vnamecomp;
            });
            CloseAct.FormOpenCommon(this, vf);
        }

        /// <summary>
        /// イベント名をエンターキーで条件データに保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxEventName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                SetFormParamToConditionFromCode();
        }

        /// <summary>
        /// フォームを閉じる時参照を消去
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EventForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            eventForm = null;
        }

        /// <summary>
        /// 左, 右クリック時にListBoxの要素を選択する。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EventContents_MouseUp(object sender, MouseEventArgs e)
        {
            isDraging = false;
            DeterminationOfInstructionGroup();

            bool isCanMenu = false;

            // マウス座標かSelectedIndicesから選択すべきアイテムのインデックスを取得
            // 何も選択されてなければ座標からインデックスを取得
            int index = 0;
            // TODO:
            if (ECSelIds.Count == 0)
            {
                index = EventContents.IndexFromPoint(e.Location);
                EventContents.SelectedIndex = index;
            }
            else
                index = ECSelIds[0];

            // 右クリックされた？
            if (e.Button == MouseButtons.Right)
            {
                isCanMenu = CheckSelectedItem(index);
                if (isCanMenu == true)
                {
                    // コンテキストメニューを表示
                    Point pos = EventContents.PointToScreen(e.Location);
                    contextMenuStrip1.Show(pos);
                }
            }
        }

        /// <summary>
        /// 一つの選択ごとに命令群をまとめて選択
        /// </summary>
        public void DeterminationOfInstructionGroup()
        {
            foreach (var a in ECSelIds)
                DeterminationOfInstructionGroupAllSelect((int)a);
        }

        /// <summary>
        /// 一つの命令群をまとめて選択する処理
        /// </summary>
        /// <param name="idx"></param>
        private void DeterminationOfInstructionGroupAllSelect(int idx)
        {
            int ridx = idx;
            if (idx == Codes.Count - 1)
                return;

            if (GetCheckBoolean(ridx) == false)
                return;

            EventContents.SetSelected(ridx, true);
            ridx++;

            // 複数行の命令を選択
            string str = (string)Items[ridx];
            while (str[square_Stack] == '＿' || str[square_Stack] == '　')
            {
                EventContents.SetSelected(ridx, true);
                ridx++;
                str = (string)Items[ridx];
            }
        }


        private bool CheckSelectedItem(int idx)
        {
            if (idx == -1) return false;

            bool retbl = GetCheckBoolean(idx);
            
            string str = (string)Items[idx];
            string sstr = CollectionTool.RemoveChar(str, '　');
            if (sstr != "＿") return true;
            else return retbl;
        }


        private bool GetCheckBoolean(int index)
        { 
            // インデックスが取得できたら
            if (index >= 0)
            {
                // 接頭記号が＿のときはなにもしない。
                string st = (string)Items[index];
                // しかし◆のみだと何もしない
                string s = CollectionTool.RemoveChar(st, '　');
                if (s == "◆" /*&& s.Length == 1*/) return false;

                int cnt = 0;
                while (st[cnt] == '　' && cnt < st.Length)
                {
                    cnt++;
                }

                if (st[cnt] == '＿')
                    return false;

                // ◆★の深さを保存
                square_Stack = cnt;
            }
            return true;
        }

        /// <summary>
        /// 右クリック時に◆★の深さを保存する。
        /// </summary>
        int square_Stack = 0;

        /// <summary>
        /// 編集。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 編集ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // 一行を解析して編集フォームをオープン
            InstructionAnalysis.OpenEditForm(Codes[ECSelIds[0]]);
        }

        /// <summary>
        /// 挿入。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 挿入ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // イベント選択フォームを開く
            OpenEventSelectForm();
        }

        /// <summary>
        /// 削除。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 削除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteLine();
        }

        /// <summary>
        /// 命令の塊の削除
        /// </summary>
        public void DeleteLine()
        {
            if (CheckStckDown() == true)
                return;

            // 選択している最後の行に空白行が選択されている場合、選択解除
            if (CollectionTool.RemoveChar(Codes[ECSelIds[ECSelIds.Count - 1]], '\t') == "")
            {
                EventContents.SetSelected(ECSelIds[ECSelIds.Count - 1], false);
                if (ECSelIds.Count == 0) return;
            }

            int lidx = ECSelIds[0];
            // 選択している最初の行が複数行の命令の途中の場合、選択解除
            string str = (string)Items[lidx];
            if (str[0] == '＿')
            {
                EventContents.SelectedIndex = -1;
                return;
            }

            foreach (var a in ECSelIds)
                DeleteSelectLine((int)a);
            DeleteDeleteSignLine();
            
            EventContents.SelectedIndex = lidx;
            DeterminationOfInstructionGroup();
        }

        /// <summary>
        /// 最初のスタックより少なくなったら削除を取りやめる。
        /// また、不連続な選択のときも取りやめる。
        /// 空白を検知したとき◆ -> ＿ -> ◆ではない行がある場合やめる。
        /// </summary>
        private bool CheckStckDown()
        {
            int result = -1;
            int stck = -1;
            int bfint = -1;
            foreach (var a in ECSelIds)
            {
                string nostr = Codes[(int)a];
                nostr = CollectionTool.RemoveChar(nostr, '\t');
                /// 空白を検知したとき◆★ -> ＿ -> ◆★ではない行がある場合やめる。
                if (nostr.Length == 0)
                {
                    if ((int)a + 2 < Items.Count)
                    {
                        string s1 = CollectionTool.RemoveChar(((string)Items[(int)a]), '　');
                        string s2 = CollectionTool.RemoveChar(((string)Items[(int)a + 1]), '　').Substring(0, 1);
                        string s3 = CollectionTool.RemoveChar(((string)Items[(int)a + 2]), '　').Substring(0, 1);
                        if (!(s1 == "◆" || s1 == "★") && s2 == "＿" && (s3 == "◆" || s3 == "★"))
                            return true;
                    }
                    else
                        return true;
                }

                /// また、不連続な選択のときも取りやめる。
                if (bfint != -1 && (int)a - bfint != Math.Abs(1))
                    return true;

                string st = (string)Items[(int)a];

                int n = (st.IndexOf('◆') != -1) ? st.IndexOf('◆') : st.IndexOf('★');
                int m = st.IndexOf('＿');
                result = (n > m) ? n : m;
                
                bfint = (int)a;
                if (stck == -1)
                    stck = result;
                /// 最初のスタックより少なくなったら削除を取りやめる。
                if (stck > result) return true;
            }
            return false;
        }

        /// <summary>
        /// 削除するcode行に"DeleteLine"を置く
        /// </summary>
        /// <param name="idx"></param>
        private void DeleteSelectLine(int idx)
        {
            int ridx = idx;
            if (idx == Codes.Count - 1)
                return;

            Codes[ridx] = "DeleteLine";
            ridx++;
            

            // 複数行の命令を消去。
            // フォーム内容の情報からCodeを削除
            string str = (string)Items[ridx];
            while (str[square_Stack] == '＿')
            {
                Codes[ridx] = "DeleteLine";
                ridx++;
                str = (string)Items[ridx];
                string s = CollectionTool.RemoveChar(str, '　');
                if (s.Length > 2) return;
            }
        }

        /// <summary>
        /// "DeleteLine"行を削除する。
        /// </summary>
        private void DeleteDeleteSignLine()
        {
            for (int i = 0; i < Codes.Count; i++)
            {
                string str = Codes[i];
                if (str.Length < 10)
                    continue;
                else
                    str = str.Substring(0, 10);

                if (str.IndexOf("DeleteLine") == 0)
                {
                    Codes.RemoveAt(i);
                    i--;
                }
            }
            SetCodeToContentsFromCode();
        }
        

        //クリップボードのデータを取得する
        //クリップボードにデータが無いときはnullを返す
        IDataObject iData = Clipboard.GetDataObject();
        string clipbackup;
        /// <summary>
        /// 選択範囲のcodeをコピーする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void コピーToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CopyCodes();
        }

        /// <summary>
        /// 選択範囲のcodeを切り取る
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 切り取りToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (CopyCodes() == true)
                DeleteLine();
        }

        private bool CopyCodes()
        {
            if (CheckStckDown() == true)
                return false;

            // 最後の空白行が選択されている場合、選択解除
            if (CollectionTool.RemoveChar(Codes[ECSelIds[ECSelIds.Count - 1]], '\t') == "")
            {
                EventContents.SetSelected(ECSelIds[ECSelIds.Count - 1], false);
                if (ECSelIds.Count == 0)
                    return false;
            }

            string str = "";
            for (int i = 0; i < ECSelIds.Count; i++)
            {
                int j = ECSelIds[i];
                str += Codes[j] + '\r';
            }
            Clipboard.SetText(CollectionTool.RemoveChar(str, '\t'));
            clipbackup = str;

            return true;
        }

        /// <summary>
        /// コピーされたcodeを貼り付ける
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 貼り付けToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PasteCode();
        }

        /// <summary>
        /// 会話文の名前を取得。
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private string GetTalkName(string str)
        {
            if (str.Length < 2)
            {
                if (str.Length == 2)
                    return "";

                return null;
            }

            int n0 = str.IndexOf("/", 0);
            int n1 = str.IndexOf("/", 1);
            if (n0 < 0) {
                return null;
            }

            if (n1 < 0)
                throw new Exception("2個めの'/'がありません。");

            string name = str.Substring(n0 + 1, n1 - 1 - n0);
            return name;
        }

        /// <summary>
        /// 会話文の本文を取得。
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private string GetTalkString(string str, int namelength)
        {
            int c = 1;
            if (namelength != -1)
            {
                if (str.Length < 3)
                    return "";
                c = 2;
            }

            int n0 = namelength + c;
            int n1 = str.Length;
            if (n0 < 0 || n1 <= n0)
                return "";

            string ret = str.Substring(n0, n1 - n0);
            return ret;
        }

        /// <summary>
        /// キャラクター会話の場合、一発で命令生成するぞ！
        /// </summary>
        private string GetConvertToCharaTalkStrs(string cdata)
        {
            string strs = "";

            cdata = CollectionTool.RemoveChar(cdata, '\n');
            string[] cstrs = cdata.Split('\r');

            for (int i = 0; i < cstrs.Length; i++)
            {
                if (cstrs[i] == "")
                    continue;

                string name = null;
                string word = null;
                name = GetTalkName(cstrs[i]);
                int namelength = (name == null) ? -1 : name.Length;
                word = GetTalkString(cstrs[i], namelength);
                name = (namelength != -1) ? "CharaName(\"" + name + "\")" : "";
                word = (namelength != -1) ? "Text(\"" + word + "\")" : "SubT(\"" + word + "\")";
                if ( namelength != -1)
                {
                    strs += name;
                    strs += "\r";
                }
                if (word != null && word != "")
                {
                    strs += word;
                    strs += "\r";
                }
            }
            return strs;
        }

        private void PasteCode()
        {
            // 選択している最初の行が複数行の命令の途中の場合、選択解除
            string str = (string)Items[ECSelIds[0]];
            if (str[0] == '＿')
            {
                EventContents.SelectedIndex = -1;
                return;
            }

            string clpdata = (string)iData.GetData(DataFormats.UnicodeText);
            int mode = CheckClipData(clpdata);
            if (clipbackup == null && mode == -1) return;

            if (clpdata == null) clpdata = clipbackup;

            if (mode == 0)
                clpdata = GetConvertToCharaTalkStrs(clpdata);

            clpdata = AddTabForPasteCode(clpdata);

            int idx = ECSelIds[0];
            //string clipdata = clipbackup;// (string)iData.GetData(DataFormats.UnicodeText);
            List<string> strs = new List<string>();
            strs.AddRange(clpdata.Split('\r'));
            strs.RemoveAt(strs.Count - 1);
            Codes.InsertRange(ECSelIds[0], strs.ToArray());
            SetCodeToContentsFromCode();

            // グループ選択
            EventContents.ClearSelected();
            EventContents.SetSelected(idx, true);
            DeterminationOfInstructionGroup();
        }

        /// <summary>
        /// CodeをStrsとして表示可能な体にする。
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private string AddTabForPasteCode(string str)
        {
            if (str.Length == 0) return null;

            str = CollectionTool.RemoveChar(str,'\n');
            string[] strs = str.Split('\r');
            for (int i = 0; i < strs.Length; i++)
                if (strs[i].Length > 0 && strs[i][0] != '\t')
                    strs[i] = strs[i].Insert(0, "\t");

            string retstr = "";
            for (int i = 0; i < strs.Length; i++)
                if (i != strs.Length - 1 || strs[i].Length != 0)
                    retstr += strs[i] + "\r";
            
            return retstr;
        }

        /// <summary>
        /// 貼り付け可能な命令かチェック(先頭のみ)
        /// </summary>
        /// <param name="dat"></param>
        /// <returns></returns>
        private int CheckClipData(string dat)
        {
            if (dat == null || dat.Length == 0) return -1;

            string[] strs = dat.Split('\r');
            int instid = InstructionAnalysis.SeachWord(strs[0]);

            return (instid != -1) ? 1 : 0;
        }


        /// <summary>
        /// タイマー処理でイベントコンテンツの操作を行う。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (isDraging == false)
                EventContents_KeyDownAtTimer();
        }

        bool del;
        bool C;
        bool X;
        bool V;
        bool bdel;
        bool bC;
        bool bX;
        bool bV;
        /// <summary>
        /// キーごとのアクション
        /// del:削除
        /// ctrl + c:コピー
        /// ctrl + x:切り取り
        /// ctrl + v;貼り付け
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EventContents_KeyDownAtTimer()
        {
            // 誤動作防止
            if (Form.ActiveForm != this)
                return;

            del = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.Delete);
            C = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.C);
            X = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.X);
            V = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.V);

            bool isCanKey = ((ECSelIds.Count > 0) ? 
                              CheckSelectedItem(ECSelIds[0]) : false);
            if (isCanKey != true)
            {
                bdel = del;
                bC = C;
                bX = X;
                bV = V;
                return;
            }

            if (del == true && del != bdel)
            {
                bool isCanMenu = false;
                int index = EventContents.SelectedIndex;

                isCanMenu = CheckSelectedItem(index);
                if (isCanMenu == true)
                {
                    DeleteLine();
                }
            }

            if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) == true ||
                System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl) == true)
            {
                if (C == true && bC != C)
                {
                    CopyCodes();
                }
                if (X == true && bX != X)
                {
                    if (CopyCodes() == true)
                        DeleteLine();
                }
                if (V == true && bV != V)
                {
                    PasteCode();
                }
            }
            bdel = del;
            bC = C;
            bX = X;
            bV = V;
        }

        bool isDraging = false;
        private void EventContents_MouseDown(object sender, MouseEventArgs e)
        {
            isDraging = true;
        }
    }
}
